#include <Wire.h>

//2-Way motor control

int motorPin1 =  3;    // One motor wire connected to digital pin 5
int motorPin2 =  11;    // One motor wire connected to digital pin 6

int control = 8;





String inString;
int motor_pulse_val;




void setup()   {                
  // initialize the digital pins as an output:
  pinMode(motorPin1, OUTPUT); 
  pinMode(motorPin2, OUTPUT);
  
  pinMode(control, OUTPUT);  
  digitalWrite(control, LOW);

  Serial.begin(115200);
  digitalWrite(control, true);



}

// the loop() method runs over and over again,
// as long as the Arduino has power
void loop()                     
{

  
 while (Serial.available()==0)  {
    
  }

  while (Serial.available()) {
    char inChar = Serial.read();
    inString += inChar; 
    if (inChar == '\n') {
 
        Serial.write("hello\n");
        motor_pulse_val=(getValue(inString, ':', 0)).toInt();
         

         if(motor_pulse_val<0)
        {
          rotateKPPBackward(-motor_pulse_val);
        }
        else
        {
          rotateKPPForward(motor_pulse_val);
        }
        
    
       
      inString = ""; 

      }
    }
  }
String getValue(String data, char separator, int index)
{
 int found = 0;
  int strIndex[] = {
0, -1  };
  int maxIndex = data.length()-1;
  for(int i=0; i<=maxIndex && found<=index; i++){
  if(data.charAt(i)==separator || i==maxIndex){
  found++;
  strIndex[0] = strIndex[1]+1;
  strIndex[1] = (i == maxIndex) ? i+1 : i;
  }
 }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}
void rotateKPPForward(int speedOfRotate){
  analogWrite(motorPin1, speedOfRotate); //rotates motor
  analogWrite(motorPin2, 0);   // set the Pin motorPin2 LOW
  
}

void rotateKPPBackward(int speedOfRotate){
  analogWrite(motorPin2, speedOfRotate); //rotates motor
  analogWrite(motorPin1, 0);     // set the Pin motorPin1 LOW
 
}



