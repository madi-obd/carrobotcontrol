#include <Wire.h>
#include <PID_v1.h>

#include <Encoder.h>

//Define Variables we'll be connecting to
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
//PID myPID(&Input, &Output, &Setpoint,5,4,0, DIRECT);
PID myPID(&Input, &Output, &Setpoint,2,8,0, DIRECT);
//2-Way motor control

int motorPin1 =  3;    // One motor wire connected to digital pin 5
int motorPin2 =  11;    // One motor wire connected to digital pin 6
int brakePin1 = 9;
int brakePin2 = 10;
int kppPin1 = 5;
int kppPin2 = 6;
int control = 8;
int brakeControl =13;
int kppControl = 12;

int kpp_motor_pulse_val=0;

double angle=0;
int levelOfRotation = 2;

String inString;
int motor_pulse_val;
//////////////////////////////////////Must be changed
Encoder knobLeft(2, 7);


void setup()   {                
  // initialize the digital pins as an output:
  pinMode(motorPin1, OUTPUT); 
  pinMode(motorPin2, OUTPUT);  
  pinMode(control, OUTPUT);  
  digitalWrite(control, LOW);

  Serial.begin(115200);
  digitalWrite(control, true);

//inti kpp
  pinMode(kppPin1, OUTPUT); 
  pinMode(kppPin2, OUTPUT);  
  pinMode(kppControl, OUTPUT);  
  digitalWrite(kppControl, LOW);
  digitalWrite(kppControl, true);

  //init brakes controller
  pinMode(brakePin1, OUTPUT); 
  pinMode(brakePin2, OUTPUT);  
  pinMode(brakeControl, OUTPUT);  
  digitalWrite(brakeControl, LOW);
  digitalWrite(brakeControl, true);
  
  Wire.begin();

  TCCR2B|= _BV(CS20);
  TCCR2B|= _BV(CS21);
  TCCR2B&= ~_BV(CS22);

  Input = 0;
  Setpoint = 0;

  //turn the PID on
  myPID.SetMode(AUTOMATIC);

        Wire.beginTransmission(0b10010000>>1);
        Wire.write(byte(1<<6));
        Wire.write(byte(7*3.2));
        Wire.endTransmission();

  myPID.SetOutputLimits(-100, 100);
}

// the loop() method runs over and over again,
// as long as the Arduino has power
void loop()                     
{
  
 while (Serial.available()==0)  {
    
  }
  while (Serial.available()) {
    char inChar = Serial.read();
    inString += inChar; 
    if (inChar == '\n') 
    {
 
          angle=knobLeft.read();
          angle=angle*1.5;
          motor_pulse_val=(getValue(inString, ':', 0)).toInt();
          Setpoint=(getValue(inString, ':', 1)).toInt();
          kpp_motor_pulse_val=(getValue(inString, ':', 2)).toInt();
          Input=angle;
          myPID.Compute();
         // Serial.print("angle = ");       
         // Serial.print(angle);
         // Serial.println(  "\n");
          if(Output<0)
          {
            rotateSteeringRight(-Output);
          }
          else
          {
            rotateSteeringLeft(Output);
          }
          
      if(motor_pulse_val<=0){
          brake(-motor_pulse_val*2);
          Wire.beginTransmission(0b10010000>>1);
          Wire.write(byte(1<<6));
          Wire.write(byte(7*3.2));
          Wire.endTransmission();
      }
      else
      {
         brake(0);
  
         motor_pulse_val=motor_pulse_val+30;
         if(motor_pulse_val>65){motor_pulse_val=65; Serial.println("w");}
        
         Wire.beginTransmission(0b10010000>>1);
         Wire.write(byte(1<<6));
         Wire.write(byte(motor_pulse_val));
         Wire.endTransmission();     
      }  
  
          if(kpp_motor_pulse_val<0)
          {
            rotateKPPBackward(-kpp_motor_pulse_val);
          }
          else
          {
            rotateKPPForward(kpp_motor_pulse_val);
          }
        inString = ""; 
      }
  }    
}
String getValue(String data, char separator, int index)
{
 int found = 0;
  int strIndex[] = {0, -1  };
  int maxIndex = data.length()-1;
  for(int i=0; i<=maxIndex && found<=index; i++){
  if(data.charAt(i)==separator || i==maxIndex){
  found++;
  strIndex[0] = strIndex[1]+1;
  strIndex[1] = (i == maxIndex) ? i+1 : i;
  }
 }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}
void rotateSteeringLeft(int speedOfRotate){
  analogWrite(motorPin1, speedOfRotate); //rotates motor
  analogWrite(motorPin2, 0);   // set the Pin motorPin2 LOW  
}

void rotateSteeringRight(int speedOfRotate){
  analogWrite(motorPin2, speedOfRotate); //rotates motor
  analogWrite(motorPin1, 0);     // set the Pin motorPin1 LOW
}

void brake(int val)
{
  analogWrite(brakePin2, val); //rotates motor
  analogWrite(brakePin1, 0);     // set the Pin motorPin1 LOW
}

void rotateKPPForward(int speedOfRotate){
  analogWrite(kppPin1, speedOfRotate); //rotates motor
  analogWrite(kppPin2, 0);   // set the Pin motorPin2 LOW
}

void rotateKPPBackward(int speedOfRotate){
  analogWrite(kppPin2, speedOfRotate); //rotates motor
  analogWrite(kppPin1, 0);     // set the Pin motorPin1 LOW
}
