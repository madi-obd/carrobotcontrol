/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actuators;

import robotautomaticcontrol.IControlSystemWindow;
import java.util.Enumeration;
import java.util.HashMap;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.util.TooManyListenersException;
//import sun.org.mozilla.javascript.internal.Token;

/**
 *
 * @author Admin
 */
public class ActuatorServer implements IActuatorServer, SerialPortEventListener {
    
     //passed from main GUI
    
    IControlSystemWindow windowControl = null;
    //for containing the ports that will be found
    private Enumeration ports = null;
    //map the port names to CommPortIdentifiers
    private HashMap portMap = new HashMap();

    //this is the object that contains the opened port
    private CommPortIdentifier selectedPortIdentifier = null;
    private SerialPort serialPort = null;

    //input and output streams for sending and receiving data
    private InputStream input = null;
    private OutputStream output = null;
    Writer w = null;
     //just a boolean flag that i use for enabling
    //and disabling buttons depending on whether the program
    //is connected to a serial port or not
    private boolean bConnected = false;

    //the timeout value for connecting with the port
    final static int TIMEOUT = 2000;

    //some ascii values for for certain things
    final static int SPACE_ASCII = 32;
    final static int DASH_ASCII = 45;
    final static int NEW_LINE_ASCII = 10;

    //a string for recording what goes on in the program
    //this string is written to the GUI
    String logText = "";
    
    int kppOutputPower = 0;
    
    public ActuatorServer(IControlSystemWindow window)
    {
        this.windowControl = window;
    }
    
    //search for all the serial ports
    //pre: none
    //post: adds all the found ports to a combo box on the GUI
    @Override
    public void searchForPorts()
    {
        ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements())
        {
            CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                windowControl.AddControlPortToUI(curPort.getName());            
                portMap.put(curPort.getName(), curPort);
            }
        }
    }
     
      //starts the event listener that knows whenever data is available to be read
    //pre: an open serial port
    //post: an event listener for the serial port that knows when data is recieved
    public void initListener()
    {
        try
        {
            message=new StringBuilder();
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        }
        catch (TooManyListenersException e)
        {
            logText = "Too many listeners. (" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);        
            windowControl.LogTextToUi(logText + "\n");
        }
    }
    
    
    //disconnect the serial port
    //pre: an open serial port
    //post: clsoed serial port
    @Override
    public void disconnect()
    {
        //close the serial port
        try
        {
            serialPort.removeEventListener();
            serialPort.close();
            input.close();
            output.close();
            w.close();
            setConnected(false);
             
            logText = "Disconnected.";
            windowControl.SetConsoleTextColor(Color.red);        
            windowControl.LogTextToUi(logText + "\n");
        }
        catch (Exception e)
        {
            logText = "Failed to close " + serialPort.getName() + "(" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);        
            windowControl.LogTextToUi(logText + "\n");
        }
    }

    @Override
    final public boolean getConnected()
    {
        return bConnected;
    }

    @Override
    public void setConnected(boolean bConnected)
    {
        this.bConnected = bConnected;
    }
    //what happens when data is received
    //pre: serial event is triggered
    //post: processing on the data it reads
    
    
    //open the input and output streams
    //pre: an open port
    //post: initialized intput and output streams for use to communicate data
    public boolean initIOStream()
    {
        //return value for whather opening the streams is successful or not
        boolean successful = false;

        try {
            //
            input = serialPort.getInputStream();
            output = serialPort.getOutputStream();
            w=new OutputStreamWriter(output);
            
            successful = true;
            return successful;
        }
        catch (IOException e) {
            logText = "I/O Streams failed to open. (" + e.toString() + ")";
           // window.txtLog.setForeground(Color.red);
            windowControl.LogTextToUi(logText + "\n");
            return successful;
        }
    }
    
    StringBuilder message;
    public void serialEvent(SerialPortEvent evt) {
        if (evt.getEventType() == SerialPortEvent.DATA_AVAILABLE)
        {
            try
            {
                byte singleData = (byte)input.read();

                if (singleData != NEW_LINE_ASCII)
                {
                    logText = new String(new byte[] {singleData});
                    message.append(logText);
                }
                else
                {                        
                    windowControl.LogTextToUi(message.toString()+"\n");                                       
                    message=new StringBuilder();
                }
            }
            catch (Exception e)
            {
                logText = "Failed to read data. (" + e.toString() + ")";
                windowControl.SetConsoleTextColor(Color.red);        
                windowControl.LogTextToUi(logText + "\n");
            }
            //Autoscroll Added
            windowControl.MakeAutoscrol();
        }
    }
    double steering =0;
    int MessageQount =0;
    //method that can be called to send data
    //pre: open serial port
    //post: data sent to the other device
    @Override
    public void writeData(int accel, int steering)
    {   
        this.steering = steering;
        try
        {
            MessageQount++;
            windowControl.LogTextToUi("");
            w.write(accel+":"+steering+":"+kppOutputPower+"\n");          
            w.flush();
            windowControl.LogTextToUi("From car:"+accel+":"+steering+"here\n"+"qnt= "+MessageQount+"\n");
            
        }
        catch (Exception e)
        {
            logText = "Failed to write data. (" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");
        }
    }

    @Override
    public void connect(String port) {
         
        String selectedPort = port;
        selectedPortIdentifier = (CommPortIdentifier)portMap.get(selectedPort);

        CommPort commPort = null;

        try
        {
            //the method below returns an object of type CommPort
            commPort = selectedPortIdentifier.open("TigerControlPanel", TIMEOUT);
            //the CommPort object can be casted to a SerialPort object
            serialPort = (SerialPort)commPort;
            serialPort.setSerialPortParams(115200,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
            //for controlling GUI elements
            setConnected(true);

            //logging
            logText = selectedPort + " opened successfully.";
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");

            //CODE ON SETTING BAUD RATE ETC OMITTED
            //XBEE PAIR ASSUMED TO HAVE SAME SETTINGS ALREADY

            //enables the controls on the GUI if a successful connection is made
            this.initListener();
            initIOStream();
             
        }
        catch (PortInUseException e)
        {
            logText = selectedPort + " is in use. (" + e.toString() + ")";
            
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");
        }
        catch (Exception e)
        {
            logText = "Failed to open " + selectedPort + "(" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");
        }
    }

    @Override
    public double getDataSteering()  {
         
       return steering/14.0;
        
    }

    @Override
    public void writeKPPData(int kppValue) {
        kppOutputPower = kppValue;
    }
    
        
}
