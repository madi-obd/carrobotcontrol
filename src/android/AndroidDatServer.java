/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package android;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import socketutil.JSONTransferUtil;
import socketutil.SocketHelperCommunicator;
import types.DataFromCar;
import types.DataFromControlAndroid;
/**
 *
 * @author Мади
 */
public class AndroidDatServer extends Thread implements IAndroidServer {
    
    // """
    //A JSON socket server used to communicate with a JSON socket client. All the
    //data is serialized in JSON. How to use it:
    
    String host;
    int port;
    ServerSocket server = null;   
    List<ServerThread> ServerThreads  ;
    DataFromControlAndroid dfa;
    DataFromCar dfc;

    @Override
    public DataFromCar getDfc() {
        return dfc;
    }

    @Override
    public void setDfc(DataFromCar dfc) {
        this.dfc = dfc;
    }
        
    @Override
    public DataFromControlAndroid getDfa() {
        return dfa;
    }

    @Override
    public void setDfa(DataFromControlAndroid dfa) {
        this.dfa = dfa;
    }
        
    public AndroidDatServer (String host, int port)
    {
        this.host = host;
        this.port = port; 
        ServerThreads = new ArrayList<ServerThread>();
        dfa=new DataFromControlAndroid();
        dfc = new DataFromCar();
    }
    
    @Override
    public void Start()
    {
               ServerThread st;
               st = new ServerThread( );               
               Thread t = new Thread(st);
               t.setDaemon(true);
               t.start();            
               ServerThreads.add(st );                     
    }
        
    private class ServerThread implements Runnable{
        
        Socket s;
        void Send(OutputStream outStream, DataFromCar carData)
        {

            String toAndroidJSON = JSONTransferUtil.CarDataToString(carData);

            SocketHelperCommunicator._send(outStream, toAndroidJSON.getBytes());


        }
        
        DataFromControlAndroid Receive(BufferedReader inpStream)
        {            
            String fromAndroid = SocketHelperCommunicator._recv(inpStream);            
            return JSONTransferUtil.StringToAndroidData(fromAndroid);            
        }
        
        boolean NeedTerminate;

        public void setNeedTerminate(boolean NeedTerminate) {
            this.NeedTerminate = NeedTerminate;
        }

        public boolean isNeedTerminate() {
            return NeedTerminate;
        }
        public ServerThread()
        {            
        
             // и запускаем новый вычислительный поток (см. ф-ю run())                    
        }
        int a=0;
        @Override       
        public void run()
        {
             try
            {
            server = new ServerSocket(port );
            }
            catch(Exception ex)
            {
                System.out.println("init error: "+ex);
            } 
            
            while(!isNeedTerminate()){
            try
            {
                
                s=server.accept();
                s.setTcpNoDelay(true);
            }
            catch(Exception ex)
            {
                System.out.println("init error: "+ex);
            }             
            try
            {
                InputStream is = s.getInputStream();                
                BufferedInputStream br = new BufferedInputStream(is);
                BufferedReader brr = new BufferedReader( new InputStreamReader(br));
                // и оттуда же - поток данных от сервера к клиенту
                OutputStream os = s.getOutputStream();
                while(!isNeedTerminate()){                                                 
                Send(os, dfc);                   
                dfa =  Receive(brr);                               
                System.out.println("wheel "+dfa.getSteeringWheelRate()+ " accel "+dfa.getAcceleratioRate());
                   
                }
                s.close();
                
                System.out.println("Close");             
            }
            catch(Exception e)
            {                                
                System.out.println("communicate error: "+e);} // вывод исключений
            }
        }
        }
}
