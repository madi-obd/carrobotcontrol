/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package android;

import types.DataFromCar;
import types.DataFromControlAndroid;

/**
 *
 * @author Мади
 */
public interface IAndroidServer {

    void Start();

    DataFromControlAndroid getDfa();

    DataFromCar getDfc();

    void setDfa(DataFromControlAndroid dfa);

    void setDfc(DataFromCar dfc);
    
}
