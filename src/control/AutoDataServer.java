/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;
import robotautomaticcontrol.IControlSystemWindow;
import de.fischl.usbtin.CANMessage;
import de.fischl.usbtin.CANMessageListener;
import de.fischl.usbtin.USBtin;
import de.fischl.usbtin.USBtinException;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import helpers.ByteHelper;
import java.util.Enumeration;
import java.util.HashMap;

/**
 *
 * @author Мади
 */
public class AutoDataServer implements CANMessageListener, IAutoDataServer  {
      //and disabling buttons depending on whether the program
    //is connected to a serial port or not
    private boolean bConnected = false;
    
    //for containing the ports that will be found
    private Enumeration ports = null;
    //map the port names to CommPortIdentifiers
    private HashMap portMap = new HashMap();

    //this is the object that contains the opened port
    private CommPortIdentifier selectedPortIdentifier = null;
    
     /** CAN message identifier we look for */
    
    static final int WATCHID_ENGINE_SPEED = 0x201;
    static final int WATCHID_KPP = 0x3E9;
    int speed=0, RPM=0;
    String currentKPPPosition = "P";
    
    USBtin usbtin ;
       
    IControlSystemWindow windowControl=null;
       
    public AutoDataServer(IControlSystemWindow windowControl)
    {
        this.windowControl=windowControl;       
    }
       
     @Override
    public void receiveCANMessage(CANMessage canmsg) {
        // In this example we look for CAN messages with given ID
        if (canmsg.getId() == WATCHID_ENGINE_SPEED) {
           
            for (byte b : canmsg.getData()) {
              //  System.out.print(" " + b);
                RPM=ByteHelper.ByteToUnsignedInt(canmsg.getData()[0])*256+ByteHelper.ByteToUnsignedInt(canmsg.getData()[1]) ;
                speed=(ByteHelper.ByteToUnsignedInt(canmsg.getData()[4])*256+ ByteHelper.ByteToUnsignedInt(canmsg.getData()[5]))/100;
            }                      
           // windowControl.SetRPM( RPM );           
            //windowControl.SetSpeed( speed);
                    
        } else if(canmsg.getId() == WATCHID_KPP)
        {
            String KPP = "P";
            
            if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==0){
                
                KPP="P";
            }
             if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==16){
                
                KPP="R";
            }
             
              if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==32){
                
                KPP="N";
            }
               if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==48){
                
                KPP="D";
            }
               currentKPPPosition = KPP;
        }
    }

    @Override
    public void disconnect() {
         // TODO add your handling code here:
         try {
            usbtin.closeCANChannel();
            usbtin.disconnect();
        } catch (USBtinException ex) {
             System.out.println(ex.getMessage());
        }
    }

    @Override
    public boolean getConnected() {
        return bConnected;
    }

    @Override
    public void searchForPorts() {
         ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements())
        {
            CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                windowControl.AddCanPortToUI(curPort.getName());            
                portMap.put(curPort.getName(), curPort);
            }
        }
    }

    @Override
    public void setConnected(boolean bConnected) {
         this.bConnected = bConnected; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRPM() {
        return this.RPM;
    }

    @Override
    public int getSpeed() {
        return this.speed;
    }

    @Override
    public void connect(String port) {
        // TODO add your handling code here:
         try {
             
            String selectedPort = port;
               
            CommPort commPort = null;
            // create the instances
            usbtin = new USBtin();
            
            // connect to USBtin and open CAN channel with 10kBaud in Active-Mode
            usbtin.connect(selectedPort); // Windows e.g. "COM3"
            usbtin.addMessageListener(this);            
            usbtin.openCANChannel(500000, USBtin.OpenMode.ACTIVE);

            // send an example CAN message (standard)
          //  usbtin.send(new CANMessage(0x100, new byte[]{0x11, 0x22, 0x33}));
            // send an example CAN message (extended)
          //  usbtin.send(new CANMessage(0x101, new byte[]{0x44}, true, false));

            // now wait for user input
            System.out.println("Listen for CAN messages (watch id=" + WATCHID_ENGINE_SPEED + ") ... press ENTER to exit!");
            

            // close the CAN channel and close the connection
            
        } catch (USBtinException ex) {
            
            // Ohh.. something goes wrong while accessing/talking to USBtin           
            System.err.println(ex);            
            
        } 
    }

    @Override
    public String getCurrentKPP() {
        return this.currentKPPPosition;
    }
    
    
}
