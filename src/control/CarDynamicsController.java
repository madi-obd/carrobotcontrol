/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.LinkedList;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Quaternion;
import org.neuroph.core.NeuralNetwork;

/**
 *
 * @author Admin
 */
public class CarDynamicsController implements ICarDynamicsControl {

    double acceleration = 0;
    double steeringWheel = 0;
    double carX = 0;
    double carY = 0;
    double carAngle = 0;
    int currentWayPoint = 1;
    NeuralNetwork neuralNetwork = null;

    LinkedList<Vector2> way = null;

    
    
    
    MiniPID miniPIDDistanse, miniPIDAngle; 
		
		
    
    
    
    public CarDynamicsController() {
        //  neuralNetwork = NeuralNetwork.createFromFile("C:\\Users\\Admin\\Documents\\NetBeansProjects\\CarController\\Neural Networks\\NewNeuralNetwork1.nnet");
            miniPIDDistanse = new MiniPID(1, 0.01, 0.4);
            miniPIDAngle = new MiniPID(1, 0.01, 0.4);
            
            
            miniPIDDistanse.setOutputLimits(33);
            miniPIDAngle.setOutputLimits(33);
		//miniPID.setMaxIOutput(2);
		
    }

    @Override
    public double getAcceleration() {
        return acceleration;
    }

    @Override
    public double getSteeringWheel() {
        return steeringWheel;
    }

    double last_way_angle =0;
    double last_car_angle_old =0;
    double magnitudeOfPositionChangeLimit = 2.5;
    
//    private float GetPositionMagnitudeMaxLevel(LinkedList<Vector2> waypoints, int currentWaypoint,  double speed)
//    {
//
//        float MagicDistance = 20;
//        //Get Point at distance
//        float distance = 0;
//        int pointNumAtDist = 0;
//
//        for( int i=currentWaypoint; i < waypoints.size()-2; i++)
//        {
//            Vector2 toSumMe = new Vector2(waypoints.get(i).x, waypoints.get(i).y);
//            distance += toSumMe.sub(waypoints.get(i + 1)).len();
//            pointNumAtDist = i + 1;           
//            if(distance>=MagicDistance)
//            {
//                break;
//            }
//        }
//        double angle1= GetNextWayAngle(waypoints.get(currentWaypoint), waypoints.get((int)pointNumAtDist / 2));
//        double angle2 = GetNextWayAngle(waypoints.get((int)pointNumAtDist / 2), waypoints.get(pointNumAtDist) );
//        //TODO here can be difficult bug, must be fixed in future
//        double angle = angle1-angle2;
//
//        if(Math.abs(angle)<4)
//        {
//            return 17;
//        }
//        else
//        {
//            return 4;
//        }
//
//    }
    
    @Override
    public void updatePosition(double carX, double carY, double carAngle, double carSpeed, double distInFront) {

        this.carX = carX;
        this.carY = carY;
        
        if(currentWayPoint<3)
        {
            
            carAngle = GetNextWayAngle(way.get(0), way.get(1));
        }
        this.carAngle = carAngle;
         Vector2 gpsXYPosition = new Vector2((float) carX, (float) carY);
         Vector2 distVector = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
        
         // magnitudeOfPositionChangeLimit = GetPositionMagnitudeMaxLevel(way, currentWayPoint, carSpeed);
         double dis =distVector.sub(gpsXYPosition).len();
         if (dis < 5.5) {
            currentWayPoint++;
            if (currentWayPoint == way.size()) {
                currentWayPoint = 0;
            }
        }

        int nextWaypoint = currentWayPoint + 1;
        if (nextWaypoint == way.size()) {
            nextWaypoint = 0;
        }
        int prevWayPoint = currentWayPoint - 1;
        if (prevWayPoint == -1) {
            prevWayPoint = way.size() - 1;
        }

        double nextWayAngle = GetNextWayAngle(way.get(prevWayPoint), way.get(currentWayPoint));
        
        double rtraj = last_way_angle - nextWayAngle;
        
        last_way_angle = nextWayAngle;
        
        
        double angleDelta = nextWayAngle - carAngle;

        double anglePIDOutput = this.miniPIDAngle.getOutput(carAngle, nextWayAngle);
        
        if (angleDelta < -180) {
            angleDelta = angleDelta + 2 * 180;
        } else if (angleDelta > 180) {
            angleDelta = angleDelta - 2 * 180;
        }
        double psy = angleDelta;

       
        
        Vector2 curentCopy = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
        double dist_error = GetDelta(way.get(prevWayPoint), curentCopy.sub(way.get(prevWayPoint)), gpsXYPosition).sub(gpsXYPosition).len();
        Vector2 prev_a = way.get(prevWayPoint);
        Vector2 current_b = way.get(currentWayPoint);
        Vector2 my_c = gpsXYPosition;

        if (((current_b.x - prev_a.x) * (my_c.y - prev_a.y) - (current_b.y - prev_a.y) * (my_c.x - prev_a.x)) < 0) {
            dist_error = -dist_error;
            psy = psy;
        }
        
               System.out.println("car angle = " + carAngle+ " nextwayangle =  "+ nextWayAngle+ " dist error = " + dist_error+" current waypoint = "+currentWayPoint+" dis = "+dis);

        //var a = psy - Mathf.Rad2Deg * Mathf.Atan2(0.06 * dist_error  ,rigidbody.velocity.magnitude);
        if(carSpeed<0.5)
        {
            
            carSpeed=1;
        }
        
        double psyss = Math.toRadians(1100*carSpeed*rtraj/(620*(1+1)));
        
        double angle_delta = last_car_angle_old -carAngle;
        
        last_car_angle_old = carAngle;
        double kdyaw=0.1;
        
        float coeffOfSharp = 0;
       // if (magnitudeOfPositionChangeLimit > 4)
        //{
            if (Math.abs(dist_error) > 0.6)
            {
                coeffOfSharp = 0.4f;
            }

            else if (Math.abs(dist_error) < 0.6 && Math.abs(dist_error) > 0.15)
            {
                coeffOfSharp = 0.25f;

            }
            else
            {
                coeffOfSharp = 0.1f;
            }
       // }
       // else
       // {
       //     coeffOfSharp = 0.4f;
       // }
        double distancePIDOutput = this.miniPIDDistanse.getOutput(Math.toDegrees(Math.atan2( 0.6* dist_error, carSpeed)), 0);
        
        double wheelAngle = anglePIDOutput + distancePIDOutput;
//double wheelAngle = anglePIDOutput/*-psyss*/ + Math.toDegrees(Math.atan2( 0.6* dist_error, carSpeed))/*+kdyaw*(angleDelta-rtraj)*/;
         System.out.println("psy = " + psy+ " khe = " + Math.toDegrees(Math.atan2( 0.6* dist_error, carSpeed)));
        if (wheelAngle > 33) {
            wheelAngle = 33;
        }
        if (wheelAngle < -33) {
            wheelAngle = -33;
        }

        this.steeringWheel = wheelAngle*14;
         this.acceleration=0;      
    }
    
    
//    public void updatePosition(double carX, double carY, double carAngle, double carSpeed, double distInFront) {
//
//        this.carX = carX;
//        this.carY = carY;
//        
//        if(currentWayPoint<3)
//        {
//            
//            carAngle = GetNextWayAngle(way.get(currentWayPoint-1), way.get(currentWayPoint));
//        }
//        this.carAngle = carAngle;
//         Vector2 gpsXYPosition = new Vector2((float) carX, (float) carY);
//         Vector2 distVector = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//         double dis = distVector.sub(gpsXYPosition).len() ;
//        if (dis< 4.5) {
//            currentWayPoint++;
//            if (currentWayPoint == way.size()) {
//                currentWayPoint = 0;
//            }
//        }
//
//        int nextWaypoint = currentWayPoint + 1;
//        if (nextWaypoint == way.size()) {
//            nextWaypoint = 0;
//        }
//        int prevWayPoint = currentWayPoint - 1;
//        if (prevWayPoint == -1) {
//            prevWayPoint = way.size() - 1;
//        }
//
//        double nextWayAngle = GetNextWayAngle(way.get(prevWayPoint), way.get(currentWayPoint));
//        nextWayAngle = nextWayAngle;
//        
//        
//        
//        double angleDelta = nextWayAngle - carAngle;
//
//        if (angleDelta < -180) {
//            angleDelta = angleDelta + 2 * 180;
//        } else if (angleDelta > 180) {
//            angleDelta = angleDelta - 2 * 180;
//        }
//        double psy = angleDelta;
//
//       
//        
//        Vector2 curentCopy = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//        double dist_error = GetDelta(way.get(prevWayPoint), curentCopy.sub(way.get(prevWayPoint)), gpsXYPosition).sub(gpsXYPosition).len();
//        Vector2 prev_a = way.get(prevWayPoint);
//        Vector2 current_b = way.get(currentWayPoint);
//        Vector2 my_c = gpsXYPosition;
//
//        if (((current_b.x - prev_a.x) * (my_c.y - prev_a.y) - (current_b.y - prev_a.y) * (my_c.x - prev_a.x)) < 0) {
//            dist_error = -dist_error;
//            psy = psy;
//        }
//        
//        System.out.println("car angle = " + carAngle+ " nextwayangle =  "+ nextWayAngle+ " dist error = " + dist_error+" current waypoint = "+currentWayPoint+" dis = "+dis);
//        //var a = psy - Mathf.Rad2Deg * Mathf.Atan2(0.06 * dist_error  ,rigidbody.velocity.magnitude);
//        if(carSpeed<1)
//        {
//            
//            carSpeed=1;
//        }
//        double wheelAngle = psy + Math.toDegrees(Math.atan2(0.01 * dist_error, carSpeed));
//
//        if (wheelAngle > 33) {
//            wheelAngle = 33;
//        }
//        if (wheelAngle < -33) {
//            wheelAngle = -33;
//        }
//
//        this.steeringWheel = wheelAngle*14;
//         this.acceleration=0;      
//    }
    
//    @Override
//    public void updatePosition(double carX, double carY, double carAngle, double carSpeed, double distInFront) {
//
//        this.carX = carX;
//        this.carY = carY;
//        
//        if(currentWayPoint<3)
//        {
//            
//            carAngle = GetNextWayAngle(way.get(currentWayPoint-1), way.get(currentWayPoint));
//        }
//        this.carAngle = carAngle;
//         Vector2 gpsXYPosition = new Vector2((float) carX, (float) carY);
//         Vector2 distVector = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//        if (distVector.sub(gpsXYPosition).len() < 2.5) {
//            currentWayPoint++;
//            if (currentWayPoint == way.size()) {
//                currentWayPoint = 0;
//            }
//        }
//
//        int nextWaypoint = currentWayPoint + 1;
//        if (nextWaypoint == way.size()) {
//            nextWaypoint = 0;
//        }
//        int prevWayPoint = currentWayPoint - 1;
//        if (prevWayPoint == -1) {
//            prevWayPoint = way.size() - 1;
//        }
//
//        double nextWayAngle = GetNextWayAngle(way.get(prevWayPoint), way.get(currentWayPoint));
//        nextWayAngle = nextWayAngle;
//        
//        
//        
//        double angleDelta = nextWayAngle - carAngle;
//
//        if (angleDelta < -180) {
//            angleDelta = angleDelta + 2 * 180;
//        } else if (angleDelta > 180) {
//            angleDelta = angleDelta - 2 * 180;
//        }
//        double psy = angleDelta;
//
//       
//        
//        Vector2 curentCopy = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//        double dist_error = GetDelta(way.get(prevWayPoint), curentCopy.sub(way.get(prevWayPoint)), gpsXYPosition).sub(gpsXYPosition).len();
//        Vector2 prev_a = way.get(prevWayPoint);
//        Vector2 current_b = way.get(currentWayPoint);
//        Vector2 my_c = gpsXYPosition;
//
//        if (((current_b.x - prev_a.x) * (my_c.y - prev_a.y) - (current_b.y - prev_a.y) * (my_c.x - prev_a.x)) < 0) {
//            dist_error = -dist_error;
//            psy = psy;
//        }
//        
//        System.out.println("car angle = " + carAngle+ " nextwayangle =  "+ nextWayAngle+ " dist error = " + dist_error);
//        //var a = psy - Mathf.Rad2Deg * Mathf.Atan2(0.06 * dist_error  ,rigidbody.velocity.magnitude);
//        if(carSpeed<0.5)
//        {
//            
//            carSpeed=1;
//        }
//        double wheelAngle = psy + Math.toDegrees(Math.atan2(0.2 * dist_error, carSpeed));
//
//        if (wheelAngle > 33) {
//            wheelAngle = 33;
//        }
//        if (wheelAngle < -33) {
//            wheelAngle = -33;
//        }
//
//        this.steeringWheel = wheelAngle*14;
//         this.acceleration=0;      
//    }
private static double GetNextWayAngle(Vector2 prevPoint, Vector2 nextPoint) {
        double deltaY = prevPoint.x - nextPoint.x;
        double deltaX = prevPoint.y - nextPoint.y;
        
        
        double _angle = Math.toDegrees(Math.atan2(nextPoint.x-prevPoint.x, nextPoint.y-prevPoint.y));
        if(_angle < 0 ){
         return  360+_angle ;
        }
        else
        {            
            return  _angle;
        }
    }

    private static Vector2  GetDelta(Vector2 origin, Vector2 direction, Vector2 point) {
        
        Vector2 direction1 = new Vector2(direction.x, direction.y);
        Vector2 point1 = new Vector2(point.x, point.y);
        direction1.nor();
        
        Vector2 v = point1.sub(origin);
        float d = Vector2.dot(v.x, v.y, direction1.x, direction1.y);
        Vector2 returnVecor = new Vector2(origin.x, origin.y);
        return returnVecor.add(direction1.x * d, direction1.y * d);
    }

    @Override
    public void setWay(LinkedList<Vector2> way) {
        this.way = way;
    }

    private double CalculateBreak(double distToBrake, double speed) {
        distToBrake = (distToBrake - speed * 1000 * 0.1 / 3600);

        neuralNetwork.setInput(distToBrake, speed, 50);
        //  neuralNetwork.setInput(0, 0, 0);
        // calculate network
        neuralNetwork.calculate();
        // get network output
        double[] networkOutput = neuralNetwork.getOutput();
        return (int) networkOutput[0];
    }
}
