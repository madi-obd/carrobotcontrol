/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

/**
 *
 * @author Мади
 */
public interface IAutoDataServer {
     //disconnect the serial port
    //pre: an open serial port
    //post: clsoed serial port
    void disconnect();
    void connect(String port);
    boolean getConnected();

    //search for all the serial ports
    //pre: none
    //post: adds all the found ports to a combo box on the GUI
    void searchForPorts();

    void setConnected(boolean bConnected);
    //what happens when data is received
    //pre: serial event is triggered
    //post: processing on the data it reads

    //method that can be called to send data
    //pre: open serial port
    //post: data sent to the other device
    int getRPM();
    int getSpeed();
    
    String getCurrentKPP();
    
}
