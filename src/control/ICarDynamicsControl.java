/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.badlogic.gdx.math.Vector2;
import java.util.LinkedList;

/**
 *
 * @author Мади
 */
public interface ICarDynamicsControl {
    
    double getAcceleration();
    double getSteeringWheel();
    void updatePosition(double carX, double carY, double carAngle, double carSpeed, double distInFront);
    void setWay(LinkedList<Vector2> way);
      
}
