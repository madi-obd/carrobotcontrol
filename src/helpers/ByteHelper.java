/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;
 
public class ByteHelper {
    
    public static int ByteToUnsignedInt(byte i)
    {
        
        return i>0? i :i&0xff;
        
    }
    
    public static void main(String[] args)
    {
        
        byte a=-128;
        
        System.out.println("hello test " + ByteToUnsignedInt(a));
    }
}
