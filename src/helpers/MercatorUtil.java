/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import static org.openstreetmap.gui.jmapviewer.OsmMercator.falseNorthing;
import static org.openstreetmap.gui.jmapviewer.OsmMercator.getMaxPixels;
import static org.openstreetmap.gui.jmapviewer.OsmMercator.radius;



/**
 * Created by Мади on 21.04.2015.
 */
/**
 *
 * @author anton
 */
public  class MercatorUtil {
     
    public static final double MAX_LAT = 85.05112877980659;
    public static final double MIN_LAT = -85.05112877980659;
    private static double EARTH_RADIUS = 6378137;

    public static double getMaxPixels(int aZoomlevel) {
        return EARTH_RADIUS;
    }

   
 



 public static double DDMMSSToDecimalDegrees(double data) {

        double ddmmss = ( data / 100);

        int degrees = (int) ddmmss;
        double minutesseconds = ((ddmmss - degrees) * 100) / 60.0;


        return degrees + minutesseconds;

    }


   

     
     


    /**
     * Returns the absolut number of pixels in y or x, defined as: 2^Zoomlevel *
     * TILE_WIDTH where TILE_WIDTH is the width of a tile in pixels
     *
     * @param aZoomlevel
     * @return
     */
     

    /**
     * Returns the absolut number of pixels in y or x, defined as: 2^Zoomlevel *
     * TILE_WIDTH where TILE_WIDTH is the width of a tile in pixels
     *
     * @param aZoomlevel
     * @return
     */


    /**
     * Transform longitude to pixelspace
     *
     * <p>
     * Mathematical optimization<br>
     * <code>
     * x = radius(aZoomlevel) * toRadians(aLongitude) + falseEasting(aZoomLevel)<br>
     * x = getMaxPixels(aZoomlevel) / (2 * PI) * (aLongitude * PI) / 180 + getMaxPixels(aZoomlevel) / 2<br>
     * x = getMaxPixels(aZoomlevel) * aLongitude / 360 + 180 * getMaxPixels(aZoomlevel) / 360<br>
     * x = getMaxPixels(aZoomlevel) * (aLongitude + 180) / 360<br>
     * </code>
     * </p>
     *
     * @param aLongitude
     *            [-180..180]
     * @return [0..2^Zoomlevel*TILE_SIZE[
     * @author Jan Peter Stotz
     */
    public static double LonToX(double aLongitude, int aZoomlevel) {
        double mp = (double)getMaxPixels(aZoomlevel);
        double x = (double) ((mp * (aLongitude + 180l)) / 360l);
        x = Math.min(x, mp - 1);
        return x;
    }

    /**
     * Transforms latitude to pixelspace
     * <p>
     * Mathematical optimization<br>
     * <code>
     * log(u) := log((1.0 + sin(toRadians(aLat))) / (1.0 - sin(toRadians(aLat))<br>
     *
     * y = -1 * (radius(aZoomlevel) / 2 * log(u)))) - falseNorthing(aZoomlevel))<br>
     * y = -1 * (getMaxPixel(aZoomlevel) / 2 * PI / 2 * log(u)) - -1 * getMaxPixel(aZoomLevel) / 2<br>
     * y = getMaxPixel(aZoomlevel) / (-4 * PI) * log(u)) + getMaxPixel(aZoomLevel) / 2<br>
     * y = getMaxPixel(aZoomlevel) * ((log(u) / (-4 * PI)) + 1/2)<br>
     * </code>
     * </p>
     * @param aLat
     *            [-90...90]
     * @return [0..2^Zoomlevel*TILE_SIZE[
     * @author Jan Peter Stotz
     */
    public static double LatToY(double aLat, int aZoomlevel) {
        if (aLat < MIN_LAT)
            aLat = MIN_LAT;
        else if (aLat > MAX_LAT)
            aLat = MAX_LAT;
        double sinLat = Math.sin(Math.toRadians(aLat));
        double log = Math.log((1.0 + sinLat) / (1.0 - sinLat));
        double mp = getMaxPixels( aZoomlevel);
        double y = (double) (mp * (0.5 - (log / (4.0 * Math.PI))));
        y = (double)Math.min(y, mp - 1);
        return y;
    }

    /**
     * Transforms pixel coordinate X to longitude
     *
     * <p>
     * Mathematical optimization<br>
     * <code>
     * lon = toDegree((aX - falseEasting(aZoomlevel)) / radius(aZoomlevel))<br>
     * lon = 180 / PI * ((aX - getMaxPixels(aZoomlevel) / 2) / getMaxPixels(aZoomlevel) / (2 * PI)<br>
     * lon = 180 * ((aX - getMaxPixels(aZoomlevel) / 2) / getMaxPixels(aZoomlevel))<br>
     * lon = 360 / getMaxPixels(aZoomlevel) * (aX - getMaxPixels(aZoomlevel) / 2)<br>
     * lon = 360 * aX / getMaxPixels(aZoomlevel) - 180<br>
     * </code>
     * </p>
     * @param aX
     *            [0..2^Zoomlevel*TILE_WIDTH[
     * @return ]-180..180[
     * @author Jan Peter Stotz
     */
    public static double XToLon(double aX, int aZoomlevel) {
        return ((360d * aX) / getMaxPixels(aZoomlevel)) - 180.0;
    }

    /**
     * Transforms pixel coordinate Y to latitude
     *
     * @param aY
     *            [0..2^Zoomlevel*TILE_WIDTH[
     * @return [MIN_LAT..MAX_LAT] is about [-85..85]
     */
      public static int falseNorthing(int aZoomlevel) {
        return (int)(-1 * getMaxPixels(aZoomlevel) / 2);
    }
       public static double radius(int aZoomlevel) {
        return (EARTH_RADIUS ) / (2.0 * Math.PI);
    }
    public static double YToLat(double aY, int aZoomlevel) {
        aY += falseNorthing(aZoomlevel);
        double latitude = (Math.PI / 2) - (2 * Math.atan(Math.exp(-1.0 * aY / radius(aZoomlevel))));
        return -1 * Math.toDegrees(latitude);
    }

    
      /**
     * Gets the distance using Spherical law of cosines.
     *
     * @param la1 the Latitude in degrees
     * @param lo1 the Longitude in degrees
     * @param la2 the Latitude from 2nd coordinate in degrees
     * @param lo2 the Longitude from 2nd coordinate in degrees
     * @return the distance
     * @author Jason Huntley
     */
    public static double getDistance(double la1, double lo1, double la2, double lo2) {
         double aStartLat = Math.toRadians(la1);
        double aStartLong = Math.toRadians(lo1);
        double aEndLat =Math.toRadians(la2);
        double aEndLong = Math.toRadians(lo2);

        
        
        double a=Math.sin(aStartLat), b=Math.sin(aEndLat), c=Math.cos(aStartLat)
                , d=Math.cos(aEndLat), e =  Math.cos(aEndLong - aStartLong), f =a * b, n =c* d
                *e ;
        double distance = Math.acos(f
                + n);
        
        if(Double.isNaN(distance))
        {
         distance = 0;   
            
        }

        return (EARTH_RADIUS * distance);
    }
    
    public static double GetGeoAngle(double lat1, double lon1, double lat2, double lon2) {
        double G = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
        return G;
    }
    
    public static double bearing(double lat1, double lon1, double lat2, double lon2){
        double longitude1 = lon1;
        double longitude2 = lon2;
        double latitude1 = Math.toRadians(lat1);
        double latitude2 = Math.toRadians(lat2);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);

        return (Math.toDegrees(Math.atan2(y, x))+360)%360;
      }
    public static double GetDirection(double lat1, double lon1, double lat2, double lon2) {

        double x = (Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
        double y = Math.sin(lon2 - lon1) * Math.cos(lat2);
        double br = 360 - Math.toDegrees(Math.atan2(y, x));
        if (br > 360) {
            br = br - 360;

        }
        return br;

    }

}


