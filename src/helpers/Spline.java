/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;
import com.badlogic.gdx.math.Vector2;

public class Spline{

     
	/// <summary>
	/// Вычисляет узловые точки дискретного N-периодического сплайна с векторными коэфициентами.
	/// </summary>
	/// <param name="aPoints">Полюса сплайна (исходные точки). Должно быть не менее 2-х полюсов.</param>
	/// <param name="r">Порядок сплайна.</param>
	/// <param name="n">Число узлов между полюсами сплайна.</param>
	/// <param name="aIsIncludeOriginalPoints">True - сплайн будет проходить через полюса, false - сплайн не будет проходить через полюса.</param>
	/// <returns></returns>
	public static Vector2[] Calculate(Vector2[] aPoints, int r, int n , boolean aIsIncludeOriginalPoints) {
		if (aPoints == null) {
			throw new ArithmeticException("aPoints");
		}
		
		if (aPoints.length <= 2) {
			throw new ArithmeticException("Число полюсов должно быть > 2.");
		}
		
		if (r <= 0) {
			throw new ArithmeticException("Порядок сплайна должен быть > 0.");
		}
		
		if (n < 1) {
			throw new ArithmeticException("Число узлов между полюсами сплайна должно быть >= 1.");
		}
		
		int m = aPoints.length;
		int N = n * m;
		
		Vector2[] vectors;
		if (aIsIncludeOriginalPoints) {
			vectors = RecalculateVectors(aPoints, r, n, m);
		} else {
			vectors = new Vector2[m];
			vectors=aPoints.clone();
		}
		
		float[] qSpline = CalculateQSpline(n, m);
		Vector2[] resultPoints = CalculateSSpline(vectors, qSpline, r, n, m);
		
		return resultPoints;
	}
	
	/// <summary>
	/// Вычисляет вектора дискретного периодического сплайна с векторными коэфициентами, согласно
	/// формулам http://www.math.spbu.ru/ru/mmeh/AspDok/pub/2010/Chashnikov.pdf (страница 7).
	/// </summary>
	/// <param name="vectors"></param>
	/// <param name="qSpline"></param>
	/// <param name="r"></param>
	/// <param name="n"></param>
	/// <param name="m"></param>
	/// <returns></returns>
	private static Vector2[] CalculateSSpline(Vector2[] aVectors, float[] aQSpline, int r, int n, int m) {            
	        int N = n * m;
		Vector2[][] sSpline = new Vector2[r + 1][];
		for (int i = 1; i <= r; ++i) {
			sSpline[i] = new Vector2[N];
		      
		}
		
		for (int j = 0; j < N; ++j) {
			sSpline[1][j] = new Vector2(0, 0);
			for (int p = 0; p < m; ++p) {
				sSpline[1][j].x += aVectors[p].x * aQSpline[GetPositiveIndex(j - p * n, N)];
                                sSpline[1][j].y += aVectors[p].y* aQSpline[GetPositiveIndex(j - p * n, N)];
			}
		}
		
		for (int v = 2; v <= r; ++v) {
			for (int j = 0; j < N; ++j) {
				sSpline[v][j] = new Vector2(0, 0);
				for (int k = 0; k < N; ++k) {
					sSpline[v][j].x += aQSpline[k] * sSpline[v - 1][GetPositiveIndex(j - k, N)].x;
                                        sSpline[v][j].y += aQSpline[k] * sSpline[v - 1][GetPositiveIndex(j - k, N)].y;
                                        sSpline[v][j].x += aQSpline[k] * sSpline[v - 1][GetPositiveIndex(j - k, N)].x;
                                        sSpline[v][j].y += aQSpline[k] * sSpline[v - 1][GetPositiveIndex(j - k, N)].y;
				}
				sSpline[v][j].x /= n;
                                sSpline[v][j].y /= n;
			}
		}
		
		return sSpline[r];
	}        
	
	/// <summary>
	/// Вычисляет коэфициенты дискретного периодического Q-сплайна 1-ого порядка, согдасно 
	/// формулам http://www.math.spbu.ru/ru/mmeh/AspDok/pub/2010/Chashnikov.pdf (страница 6).
	/// </summary>
	/// <param name="n">Число узлов между полюсами.</param>
	/// <param name="m">Число полюсов.</param>
	/// <returns>Коэфициенты дискретного периодического Q-сплайна 1-ого порядка.</returns>
	private static float[] CalculateQSpline(int n, int m) {
		int N = n * m;
		float[] qSpline = new float[N];
		
		for (int j = 0; j < N; ++j) {
			if (j >= 0 && j <= n - 1) {
				qSpline[j] = (1.0f * n - j) / n;
			}
			if (j >= n && j <= N - n) {
				qSpline[j] = 0;
			}
			if (j >= N - n + 1 && j <= N - 1) {
				qSpline[j] = (1.0f * j - N + n) / n;
			}
		}
		
		return qSpline;
	}
	
	/// <summary>
	/// Пересчитывает коэфициенты сплайна для того, чтобы результирующий сплайн проходил через полюса.
	/// http://dha.spb.ru/PDF/discreteSplines.pdf (страница 6 и 7). 
	/// </summary>
	/// <param name="aPoints">Исходные точки.</param>
	/// <param name="r">Порядок сплайна.</param>
	/// <param name="n">Количество узлов между полюсами сплайна.</param>
	/// <param name="m">Количество полюсов.</param>
	/// <returns></returns>
	private static Vector2[] RecalculateVectors(Vector2[] aPoints, int r, int n, int m) {
		int N = n * m;
		
		// Вычисляем знаменатель.
		float[] tr = new float[m];
		tr[0] = 1;            
		for (int k = 1; k < m; ++k) {
			for (int q = 0; q < n; ++q) {
				tr[k] += Math.pow(2 * n * Math.sin((Math.PI * (q * m + k)) / N), -2 * r);
			}
			tr[k] *= Math.pow(2 * Math.sin((Math.PI * k) / m), 2 * r);
		}
		
		// Вычисляем числитель.
		Vector2[] zre = new Vector2[m];
		Vector2[] zim = new Vector2[m];
		for (int j = 0; j < m; ++j) {
			zre[j] = new Vector2(0, 0);
			zim[j] = new Vector2(0, 0);
			for (int k = 0; k < m; ++k) {
				zre[j].x+= aPoints[k].x * Math.cos((-2 * Math.PI * j * k) / m);
                                zre[j].y+= aPoints[k].y * Math.cos((-2 * Math.PI * j * k) / m);
				zim[j].x += aPoints[k].x * Math.sin((-2 * Math.PI * j * k) / m);
                                zim[j].y += aPoints[k].y * Math.sin((-2 * Math.PI * j * k) / m);
			}
		}
		
		// Считаем результат.
		Vector2[] result = new Vector2[m];
		for (int p = 0; p < m; ++p) {
			result[p] = new Vector2(0, 0);
			for (int k = 0; k < m; ++k) {
				double x= (zre[k].x * Math.cos((2 * Math.PI * k * p) / m)) - (zim[k].x * Math.sin((2 * Math.PI * k * p) / m));
                                double y= (zre[k].y * Math.cos((2 * Math.PI * k * p) / m)) - (zim[k].y * Math.sin((2 * Math.PI * k * p) / m));
				x*= 1.0f / tr[k];
                                y*= 1.0f / tr[k];
                               
                                
				result[p].x += x;
                                result[p].y += y;
			}
			result[p].x /= m;
                        result[p].y /= m;
		}
		
		return result;
	}
	
	/// <summary>
	/// Обеспечивает периодичность для заданного множества.
	/// </summary>
	/// <param name="j">Индекс элемента.</param>
	/// <param name="N">Количество элементов.</param>
	/// <returns>Периодический индекс элемента.</returns>
	private static int GetPositiveIndex(int j, int N) {
		if (j >= 0) {
			return j % N;
		}
		
		return N - 1 + ((j + 1) % N);
	}
}

