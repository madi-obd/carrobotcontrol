/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author Мади
 */
public class VectorGeo2 {
        public double lat;
        public double lon;

        /// <summary>
        /// Конструктор по-умолчанию.
        /// </summary>
        public VectorGeo2() {
            this.lat = 0;
            this.lon = 0;
        }

        /// <summary>
        /// Конструктор. Принимает координаты.
        /// </summary>
        /// <param name="x">Координата Х.</param>
        /// <param name="y">Координата Y.</param>
        public VectorGeo2(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
        }        

        /// <summary>
        ///  Конструктор. Принимает Другое вектор.
        /// </summary>
        /// <param name="v">Исходный вектор.</param>
        public VectorGeo2(VectorGeo2 v) {
            lat = v.lat;
            lon = v.lon;
        }
}
