/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kpp;

import actuators.IActuatorServer;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Admin
 */
public class ChangeDriveServer implements Runnable, IChangeDriveServer{
    
    public IActuatorServer communicatorKPP=null;
    
    int needSmallGo = 0;
    int SleepTime = 500;
    
    final int MaxActiatoValue = 65; 
    String canDriveKPP = "P";
    boolean needToWork = true;
    
    String newDriveKPP = "P";

    @Override
    public String getCanDriveKPP() {
        return canDriveKPP;
    }

    @Override
    public void setCanDriveKPP(String canDriveKPP) {
        this.canDriveKPP = canDriveKPP;
    }

    @Override
    public String getNewDriveKPP() {
        return newDriveKPP;
    }

    @Override
    public void setNewDriveKPP(String newDriveKPP) {
        this.newDriveKPP = newDriveKPP;
    }
    
    @Override
    public int DecodeDrive(String drive)
    {
        int driveNumber =0;
        switch(drive)
        {
            case "P":
                driveNumber=0;
                break;
             case "R":
                driveNumber=1;
                break;
             case "N":
                driveNumber=2;
                break;
             case "D":
                 driveNumber=3;
                       
        }
        return driveNumber;
           
    }
    
    public void moveForward()
    {
        communicatorKPP.writeKPPData(MaxActiatoValue);        
    }
    
    public void moveBackward()
    {        
        communicatorKPP.writeKPPData(-MaxActiatoValue);
    }
  
     public void smallMoveForward()
    {
        communicatorKPP.writeKPPData(MaxActiatoValue);  
        try {
            Thread.sleep(SleepTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(ChangeDriveServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      public void stopMove()
    {
        communicatorKPP.writeKPPData(0);  
         try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            Logger.getLogger(ChangeDriveServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void smallMoveBackward()
    {        
        communicatorKPP.writeKPPData(-MaxActiatoValue);
        try {
            Thread.sleep(SleepTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(ChangeDriveServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
     public void run()
    {
        while(needToWork){
        if(!canDriveKPP.equals(newDriveKPP))
        {
            if(DecodeDrive(canDriveKPP)<DecodeDrive(newDriveKPP))
            {   
                moveBackward();
                needSmallGo=10;
            }
            else
            {                
                moveForward();
                needSmallGo=-10;
            }           
        }
        else
        {
            if(needSmallGo<0){
                
                smallMoveBackward();
            }
            if(needSmallGo>0)
            {
                smallMoveForward();
            }
            needSmallGo=0;
            stopMove();
                      
        }                    
    }    
    }

    @Override
    public void Stop() {
        needToWork = false;
    }

    @Override
    public IActuatorServer getActiatorServer() {
        return this.communicatorKPP;
    }

    @Override
    public void setActuatorServer(IActuatorServer actuatorServer) {
        this.communicatorKPP = actuatorServer;
    }
}
