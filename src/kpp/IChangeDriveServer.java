/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kpp;

import actuators.IActuatorServer;

/**
 *
 * @author MADI_2
 */
public interface IChangeDriveServer {

    
    void Stop();
    int DecodeDrive(String drive);

    String getCanDriveKPP();

    String getNewDriveKPP();

    void run();

    void setCanDriveKPP(String canDriveKPP);

    void setNewDriveKPP(String newDriveKPP);
    IActuatorServer getActiatorServer();
    
    void   setActuatorServer(IActuatorServer actuatorServer);
}
