/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import helpers.VectorGeo2;
import trafficobjects.Car;

/**
 *
 * @author Мади
 */
public interface INavigationServer {
     
     //disconnect the serial port
    //pre: an open serial port
    //post: clsoed serial port
    void disconnect();
    void connect(String port);
    boolean getConnected();
    
    Car getCar() ;

    void setCar(Car car);

    //search for all the serial ports
    //pre: none
    //post: adds all the found ports to a combo box on the GUI
    void searchForPorts();

    void setConnected(boolean bConnected);
    //what happens when data is received
    //pre: serial event is triggered
    //post: processing on the data it reads

    //method that can be called to send data
    //pre: open serial port
    //post: data sent to the other device
    double getLatitude();
    double getLongitude();
    double getCarX();
    double getCarY();
    double getCarAngle();
    
    String getGPRNC();
    
    void UpdateCarXYAngleFromDynamics( double speed, double wheelAngle, double dt);
    void ResetCarXYAngle();
    void SetCarXY(double x, double y, double angle);
    
    
    
   
}
