/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import com.badlogic.gdx.math.Vector2;
import helpers.VectorGeo2;

/**
 *
 * @author Мади
 */
public interface INavigatorMessageParser {
    
    VectorGeo2 parseNavigationMessage(String Message);
    
}
