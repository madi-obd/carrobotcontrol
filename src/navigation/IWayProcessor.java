/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import com.badlogic.gdx.math.Vector2;
import helpers.VectorGeo2;
import java.util.LinkedList;
import robotautomaticcontrol.forms.PointXY;
import trafficobjects.Car;

/**
 *
 * @author Мади
 */
public interface IWayProcessor {
    boolean PersistCurrentWay();
    boolean NewWay(String Name);
    void LoadWay(String Name);
    void LoadWayNormalizedFromFile(String Name);
     void LoadWayNormalizedFromFileRealGPS(String Name);
    String[] getPersistedWays();
    LinkedList<VectorGeo2> getActiveLatLonWay();
    LinkedList<PointXY> getActiveXYWay();
    LinkedList<VectorGeo2> getXYWayNormilized();
    VectorGeo2 getXYCarPositionNormilized(Car car);
    void addPointLatLon(VectorGeo2 position);  
    LinkedList<Vector2> GetWayNormilized();
    
}
