/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import robotautomaticcontrol.IControlSystemWindow;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import com.badlogic.gdx.math.Vector2;
import helpers.MercatorUtil;
import helpers.VectorGeo2;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.TooManyListenersException;
import org.lwjgl.util.vector.Vector3f;
import org.openstreetmap.gui.jmapviewer.OsmMercator;
import programcontroller.SystemManager;
import robotautomaticcontrol.Renderer;
import robotautomaticcontrol.forms.Vector3d;
import trafficobjects.Car;

/**
 *
 * @author Admin
 */
public class NavigationServer implements INavigationServer, SerialPortEventListener{
    //passed from main GUI
    Car car = null;

    @Override
    public Car getCar() {
        return car;
    }

    @Override
    public void setCar(Car car) {
        this.car = car;
    }
    IControlSystemWindow windowControl = null;
    //for containing the ports that will be found
    private Enumeration ports = null;
    //map the port names to CommPortIdentifiers
    private HashMap portMap = new HashMap();
    public String GPRMC = "";
    double carBase=2.649;
    //this is the object that contains the opened port
    private CommPortIdentifier selectedPortIdentifier = null;
    private SerialPort serialPort = null;

    //input and output streams for sending and receiving data
    private BufferedReader input = null;
    private OutputStream output = null;
    Writer w = null;
     //just a boolean flag that i use for enabling
    //and disabling buttons depending on whether the program
    //is connected to a serial port or not
    private boolean bConnected = false;

    //the timeout value for connecting with the port
    final static int TIMEOUT = 2000;

    //some ascii values for for certain things
    final static int SPACE_ASCII = 32;
    final static int DASH_ASCII = 45;
    final static int NEW_LINE_ASCII = 10;

    //a string for recording what goes on in the program
    //this string is written to the GUI
    String logText = "";
    
    double latitude, longitude;
    
    INavigatorMessageParser ggamessageParser=null;
    
    public NavigationServer (IControlSystemWindow window)
    {
        this.windowControl = window;
        this.car = new Car();
         Renderer.getRenderer().camera.setCamera(new Vector3d(car.getX(),
                    car.getY()));
    }
    
    //search for all the serial ports
    //pre: none
    //post: adds all the found ports to a combo box on the GUI
    @Override
    public void searchForPorts()
    {
        ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements())
        {
            CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                windowControl.AddNavigationPortToUI(curPort.getName());            
                portMap.put(curPort.getName(), curPort);
            }
        }
    }
     
      //starts the event listener that knows whenever data is available to be read
    //pre: an open serial port
    //post: an event listener for the serial port that knows when data is recieved
    public void initListener()
    {
        try
        {
          
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        }
        catch (TooManyListenersException e)
        {
            logText = "Too many listeners. (" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);        
            windowControl.LogTextToUi(logText + "\n");
        }
    }
    
    
    //disconnect the serial port
    //pre: an open serial port
    //post: clsoed serial port
    @Override
    public void disconnect()
    {
        //close the serial port
        try
        {
            serialPort.removeEventListener();
            serialPort.close();
                       
            setConnected(false);
             
            logText = "Disconnected.";
            windowControl.SetConsoleTextColor(Color.red);        
            windowControl.LogTextToUi(logText + "\n");
        }
        catch (Exception e)
        {
            logText = "Failed to close " + serialPort.getName() + "(" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);        
            windowControl.LogTextToUi(logText + "\n");
        }
    }

    @Override
    final public boolean getConnected()
    {
        return bConnected;
    }

    @Override
    public void setConnected(boolean bConnected)
    {
        this.bConnected = bConnected;
    }
    //what happens when data is received
    //pre: serial event is triggered
    //post: processing on the data it reads
       
    VectorGeo2 last_vector;
    int n =0;
    
    public void serialEvent(SerialPortEvent evt) {
        
        if ( evt.getEventType() == SerialPortEvent.DATA_AVAILABLE)
        {
            try
            {
                   String inputLine = "";
                   if(input.ready())
                   {                       
                       inputLine = input.readLine();
                       n++;
                   }
                    if(inputLine.indexOf("GPGGA")>=0){                        
                        String mess = inputLine.toString();                     
                       // System.out.println(mess + "+ "+n+"\n");
                        VectorGeo2 nav = WayProcessor.PraseGPGGA(mess);    
                      
                      // VectorGeo2 nav = new VectorGeo2(1, n);
                        this.latitude = nav.lat;
                        this.longitude = nav.lon;
                        windowControl.SetLatitude(latitude);
                        //windowControl.SetLongitude(longitude);
                        if(last_vector == null){                                                       
                            last_vector = new VectorGeo2(nav.lat, nav.lon);
                            if(SystemManager.getInstance().isIsAutomaticDriveMode()){
                            VectorGeo2 gp1 = SystemManager.getInstance().getWayProcessor().getActiveLatLonWay().get(0);
                            VectorGeo2 gp2 = SystemManager.getInstance().getWayProcessor().getActiveLatLonWay().get(1);                            
                            angle = MercatorUtil.bearing(gp1.lat, gp1.lon, gp2.lat, gp2.lon);
                            }
                        }
                        
                        if(MercatorUtil.getDistance(last_vector.lat, last_vector.lon, nav.lat, nav.lon)>0.3)
                        {
                            angle = MercatorUtil.bearing(last_vector.lat, last_vector.lon, nav.lat, nav.lon);                            
                            last_vector = new VectorGeo2(nav.lat, nav.lon);
                        }

                        x = MercatorUtil.LonToX(longitude, 1)/**10*/;
                        y = MercatorUtil.LatToY(latitude, 1)/**100000*/;  
                        car.setLat(latitude);
                        car.setLon(longitude);
                         if(SystemManager.getInstance().isIsAutomaticDriveMode()){
                        car.setX(SystemManager.getInstance().getWayProcessor().getXYCarPositionNormilized(car).lat);
                        car.setY(SystemManager.getInstance().getWayProcessor().getXYCarPositionNormilized(car).lon);
                        car.setDirection(angle);
                         }
                        Renderer.getRenderer().camera.setCamera(new Vector3d((float)x,
                        (float)y));                       
                    
                   // windowControl.LogTextToUi(message.toString()+"\n");                                                          
                }
            }
            catch (Exception e)
            {
                logText = "Failed to read data. (" + e.toString() + ")";
                windowControl.SetConsoleTextColor(Color.red);        
                windowControl.LogTextToUi(logText + "\n");
            }
            //Autoscroll Added
            windowControl.MakeAutoscrol();
        }
    }

    
    public void writeData(String settingsMessage)
    {
        try
        {
            windowControl.LogTextToUi("");
            w.write(settingsMessage+"\n");          
            w.flush();
            windowControl.LogTextToUi(settingsMessage+"here\n");
            
        }
        catch (Exception e)
        {
            logText = "Failed to write data. (" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");
        }
    }

    @Override
    public void connect(String port) {         
        String selectedPort = port;
        selectedPortIdentifier = (CommPortIdentifier)portMap.get(selectedPort);
        CommPort commPort = null;
        try
        {
            //the method below returns an object of type CommPort
            commPort = selectedPortIdentifier.open("TigerControlPanel", TIMEOUT);
            //the CommPort object can be casted to a SerialPort object
            serialPort = (SerialPort)commPort;
            serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            //for controlling GUI elements
            setConnected(true);

            //logging
            logText = selectedPort + "Navigaiton opened successfully.";
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");
            
            input = new BufferedReader(new InputStreamReader( serialPort.getInputStream()));
            
            output = serialPort.getOutputStream();
            String str = "";
            int i =0;
            
            while(input.ready()&&(str = input.readLine()) != null){ 
                                    System.out.println(str+ "  "+(i++)); 
                                  }

            //CODE ON SETTING BAUD RATE ETC OMITTED
            //XBEE PAIR ASSUMED TO HAVE SAME SETTINGS ALREADY

            //enables the controls on the GUI if a successful connection is made
            this.initListener();
             
        }
        catch (PortInUseException e)
        {
            logText = selectedPort + " is in use. (" + e.toString() + ")";
            
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");
        }
        catch (Exception e)
        {
            logText = "Failed to open " + selectedPort + "(" + e.toString() + ")";
            windowControl.SetConsoleTextColor(Color.red);
            windowControl.LogTextToUi(logText + "\n");
        }
    }

    @Override
    public double getLatitude() {
        return this.latitude;
    }

    @Override
    public double getLongitude() {
        return this.longitude;
    }

    
    double x = 0;
    double y = 0;
    double angle = 0;
    
    @Override
    public double getCarX() {
        return car.getX();
    }

    @Override
    public double getCarY() {
        return car.getY();
    }

    @Override
    public double getCarAngle() {
        return car.getDirection();
    }

    @Override
    public void UpdateCarXYAngleFromDynamics(double speed, double wheelAngle, double dt) {
        angle += speed*Math.tan(wheelAngle)/carBase*dt;
        car.x += speed*Math.sin(angle)*dt;
        car.y += speed*Math.cos(angle)*dt;
        
    }

    @Override
    public void ResetCarXYAngle() {
        x = 0;
        y = 0;
        angle = 0;
    }

    @Override
    public void SetCarXY(double x, double y, double angle) {
        this.x = x;
        this.y = y;
        this.angle = angle;
    }

    @Override
    public String getGPRNC() {
        return this.GPRMC;
    }
  
}
