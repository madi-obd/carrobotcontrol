/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import com.badlogic.gdx.math.Vector3;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class SplineLine {
    public List<Vector3> controlPointsList;
    
    public SplineLine() {
        controlPointsList = new LinkedList<Vector3>();
    }
    public int ClampListPos(int pos)
    {
        if (pos < 0)
        {
            pos =  controlPointsList.size() - 1;
        }

        if (pos > controlPointsList.size())
        {
            pos = 1;
        }
        else if (pos >  controlPointsList.size()- 1)
        {
            pos = 0;
        }

        return pos;
    }
    
      public Vector3 GetCatmullRomPosition(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        //The coefficients of the cubic polynomial (except the 0.5f * which I added later for performance)
        
        Vector3 p11 = new Vector3(p1.x, p1.y, p1.z);        
        Vector3 a =  p11.scl(2f);
        
        Vector3 p21 = new Vector3(p2.x, p2.y, p2.z); 
        Vector3 p01 = new Vector3(p0.x, p0.y, p0.z); 
        Vector3 b =  p21.sub( p01);
        
        Vector3 p02 = new Vector3(p0.x, p0.y, p0.z); 
        Vector3 p12 = new Vector3(p1.x, p1.y, p1.z);
        Vector3 p22 = new Vector3(p2.x, p2.y, p2.z); 
        Vector3 p31 = new Vector3(p3.x, p3.y, p3.z);
        Vector3 c =  p02.scl(2f).sub(   p12.scl(5f)).add( p22.scl(4f)).sub( p31);
        
         Vector3 p03 = new Vector3(p0.x, p0.y, p0.z); 
        Vector3 p13 = new Vector3(p1.x, p1.y, p1.z);
        Vector3 p23 = new Vector3(p2.x, p2.y, p2.z); 
        Vector3 p33 = new Vector3(p3.x, p3.y, p3.z);
        
        Vector3 d =  p03.scl(-1f).add(p13.scl(3f )).sub(  p23.scl(3f)).add( p33);

        //The cubic polynomial: a + b * t + c * t^2 + d * t^3
        Vector3 pos =  (a.add(b.scl( t)).add (c.scl( t* t)).add ( d .scl(t * t * t))).scl(0.5f);
        String str = ""+pos.x;
        
        //if(str.equals("NaN")){
        //System.out.println("x= "+pos.x+"   y= " +pos.y);
        //}
        return pos;
    }
    
    List<Vector3> path = null;

    public List<Vector3> GetcontrolPointsInterpolated()
    {

        
            if (path == null)
            {
                path = new ArrayList<Vector3>();

                //Draw the Catmull-Rom spline between the points
                for (int i = 0; i < controlPointsList.size(); i++)
                {
                    //Cant draw between the endpoints
                    //Neither do we need to draw from the second to the last endpoint
                    //...if we are not making a looping line
                    if ((i == 0 || i == controlPointsList.size() - 2 || i == controlPointsList.size() - 1))
                    {
                        continue;
                    }
                    path.addAll(getCatmullRomSpline(i));
                }
            }
        
       
        
            return path;
        }


    
     List<Vector3> getCatmullRomSpline(int pos)
    {


        List<Vector3> interpolatedPoints = new ArrayList<Vector3>();
        //The 4 points we need to form a spline between p1 and p2
        Vector3 p0 = controlPointsList.get(ClampListPos(pos - 1));
        Vector3 p1 = controlPointsList.get(pos);
        Vector3 p2 = controlPointsList.get( ClampListPos(pos + 1));
        Vector3 p3 = controlPointsList.get(ClampListPos(pos + 2));

        //The start position of the line
        Vector3 lastPos = p1;
        interpolatedPoints.add(lastPos);

        //The spline's resolution
        //Make sure it's is adding up to 1, so 0.3 will give a gap, but 0.2 will work
        float resolution = 0.15f;

        //How many times should we loop?
        int loops = (int)(1f / resolution);

        for (int i = 1; i <= loops; i++)
        {
            //Which t position are we at?
            float t = i * resolution;

            //Find the coordinate between the end points with a Catmull-Rom spline
            Vector3 newPos =  GetCatmullRomPosition(t, p0, p1, p2, p3);

             

            //Save this pos so we can draw the next line segment
            lastPos = newPos;

            interpolatedPoints.add(newPos);
        }

        return interpolatedPoints;
    }
    
    
}
