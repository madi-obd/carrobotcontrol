/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import helpers.MercatorUtil;
import helpers.VectorGeo2;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.lwjgl.util.Color;
import org.lwjgl.util.vector.Vector2f;
import org.openstreetmap.gui.jmapviewer.OsmMercator;
import programcontroller.SystemManager;
import robotautomaticcontrol.forms.PointXY;
import robotautomaticcontrol.forms.Vector3double;
import trafficobjects.Car;

/**
 *
 * @author Admin
 */
public class WayProcessor implements IWayProcessor{
    
    String currentWayName = null;
    LinkedList<VectorGeo2> geoWay = null;
    LinkedList<VectorGeo2> xyWay = null;

    public LinkedList<VectorGeo2> getGeoWay() {
        return geoWay;
    }

    public void setGeoWay(LinkedList<VectorGeo2> geoWay) {
        this.geoWay = geoWay;
    }

    @Override
    public boolean PersistCurrentWay() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean NewWay(String Name) {
        this.currentWayName = Name;
        geoWay = new LinkedList<VectorGeo2>();
        return true;
    }
    
    public static VectorGeo2 PraseGPGGA(String GPGGAString)
    {
               String strline=GPGGAString;
               String[] parts = strline.split(",");
                String shirota = parts[2]; // 3723.465874
                String p3 = parts[3]; // N
                String dolgota = parts[4]; // 12202.26954
                String p5 = parts[5]; // W   
               return new VectorGeo2(MercatorUtil.DDMMSSToDecimalDegrees( Double.parseDouble(shirota)), MercatorUtil.DDMMSSToDecimalDegrees( Double.parseDouble(dolgota)));
  
    }
    
     public static VectorGeo2 PraseGPGGAFlatFile(String GPGGAString)
    {
                String strline=GPGGAString;
                String[] parts = strline.split(Pattern.quote("|"));
                String shirota = parts[4]; // 3723.465874
                String p3 = parts[3]; // N
                String dolgota = parts[5]; // 12202.26954
                String p5 = parts[5]; // W
    
               return new VectorGeo2( Double.parseDouble(shirota),  Double.parseDouble(dolgota));
  
    }
    @Override
    public void LoadWayNormalizedFromFile(String Name)
    {
        VectorGeo2 last_pt = null;
        FileInputStream fis = null;
        BufferedReader br = null;
        geoWay = new LinkedList<VectorGeo2>();
        xyWay = new LinkedList<VectorGeo2>();
        
        try {            
            this.currentWayName = Name;
            fis = new FileInputStream(Name);
            //Construct BufferedReader from InputStreamReader
            br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            
            int i=0;
            while ((line = br.readLine()) != null) 
            {i++;
            
            if(last_pt==null)
            {                               
                last_pt = PraseGPGGA(line);
            }
//              if(i>10)
//              {
                try{
                    VectorGeo2 curent_pt = PraseGPGGA(line);
                    if(MercatorUtil.getDistance(last_pt.lat, last_pt.lon, curent_pt.lat, curent_pt.lon)>0.6){
                    geoWay.add(curent_pt);
                    last_pt = curent_pt;
                    
                    }
                }catch(Exception ex)
                {                    
                    ex.printStackTrace();
                }  
                i=0;
             // }
            }   
        } catch (Exception ex) {
            Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
               br.close();
            } catch (IOException ex) {
                Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
        
    
    }
    @Override
    public void LoadWay(String Name) {
        FileInputStream fis = null;
        BufferedReader br = null;
        geoWay = new LinkedList<VectorGeo2>();
        xyWay = new LinkedList<VectorGeo2>();
        
        try {
            
            this.currentWayName = Name;
            fis = new FileInputStream(Name);
            //Construct BufferedReader from InputStreamReader
            br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            
            int i=0;
            while ((line = br.readLine()) != null) 
            {i++;
//              if(i>10)
//              {
                try{
                    geoWay.add(PraseGPGGA(line));
                }catch(Exception ex)
                {                    
                    ex.printStackTrace();
                }  
                i=0;
             // }
            }   
        } catch (Exception ex) {
            Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
               br.close();
            } catch (IOException ex) {
                Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }

    @Override
    public String[] getPersistedWays() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedList<VectorGeo2> getActiveLatLonWay() {
        return this.geoWay;
    }

    @Override
    public LinkedList<PointXY> getActiveXYWay( ) {
        LinkedList<PointXY> way = new LinkedList<PointXY>();
        int i = 0;
        for(VectorGeo2 point : geoWay)
        {
            i++;
            
            
            // float x= OsmMercator.LonToX(point.lon, 17);
            // float y = OsmMercator.LatToY(point.lat, 17);
              
             //  x = (float)OsmMercator.XToLon((int)x, 17);
             //  y = (float)OsmMercator.YToLat((int)y, 17);
            
            double x = MercatorUtil.LonToX(point.lon, 1)/**10*/;
            double y = MercatorUtil.LatToY(point.lat, 1)/**100000*/;                            
            System.out.println("x= "+String.valueOf(x) + " y = " +String.valueOf(y));            
            way.add(new PointXY(new Vector3double(x, y), new Color(255, 0, 0), i ));               
        }
        return way;
    }

    @Override
    public void addPointLatLon(VectorGeo2 position) {
        geoWay.add(position);
    }

    @Override
    public LinkedList<VectorGeo2> getXYWayNormilized() {
       VectorGeo2 geoWayCenter = geoWay.get(0);      
       LinkedList<VectorGeo2> normilizedPositions = new LinkedList<VectorGeo2>();
       normilizedPositions.add(new VectorGeo2(0,0));
            
       for(int i = 1; i<geoWay.size(); i++)
       {           
           double dy = MercatorUtil.getDistance( geoWay.get(i).lat, geoWay.get(0).lon, geoWay.get(0).lat, geoWay.get(0).lon);   
           double dx = MercatorUtil.getDistance( geoWay.get(0).lat, geoWay.get(i).lon, geoWay.get(0).lat, geoWay.get(0).lon);           
           dy = geoWay.get(0).lat<geoWay.get(i).lat?dy:-dy;
           dx = geoWay.get(0).lon<geoWay.get(i).lon?dx:-dx;           
           VectorGeo2 activeVector = new VectorGeo2(dx, dy);           
           normilizedPositions.add(activeVector);
       }                      
        SplineLine sl = new SplineLine();                       
        for(VectorGeo2 pts : normilizedPositions){
            Vector3 vk = new Vector3((float)pts.lat,(float)pts.lon, 0.0f);
            sl.controlPointsList.add(vk);
        }       
       normilizedPositions.clear();
       
       for (Vector3 pts : sl.GetcontrolPointsInterpolated()){
            normilizedPositions.add(new VectorGeo2(pts.x, pts.y));
       }
       
       return normilizedPositions;              
    }

    @Override
    public VectorGeo2 getXYCarPositionNormilized(Car car) {
               
           //Car car = SystemManager.getInstance().getNavigationServer().getCar();

           double dy = MercatorUtil.getDistance( car.getLat(), geoWay.get(0).lon, geoWay.get(0).lat, geoWay.get(0).lon);   
           double dx = MercatorUtil.getDistance( geoWay.get(0).lat, car.getLon(), geoWay.get(0).lat, geoWay.get(0).lon);           
           dy = geoWay.get(0).lat < car.getLat() ? dy : -dy;
           dx = geoWay.get(0).lon < car.getLon() ? dx : -dx;                     
           VectorGeo2 activeVector = new VectorGeo2( dx, dy );                 
           return activeVector;
    }
    
    @Override
    public LinkedList<Vector2> GetWayNormilized() {
        LinkedList<Vector2> way = new LinkedList<Vector2>();       
        LinkedList<VectorGeo2> wayBig = getXYWayNormilized();      
        for (int i = 0; i<wayBig.size(); i++)
        {
            way.add(new Vector2((float)wayBig.get(i).lat, (float)wayBig.get(i).lon));          
        }       
        return way;
    }   

    String lastLine="";
    
    
    
    
    @Override
    public void LoadWayNormalizedFromFileRealGPS(String Name) {
        FileInputStream fis = null;
        BufferedReader br = null;
        lastLine="";
        geoWay = new LinkedList<VectorGeo2>();
        xyWay = new LinkedList<VectorGeo2>();        
        try {            
            this.currentWayName = Name;
            fis = new FileInputStream(Name);
            //Construct BufferedReader from InputStreamReader
            br = new BufferedReader(new InputStreamReader(fis));
            String line = null;            
            int i=0;
            while ((line = br.readLine()) != null) 
            {i++;
             if(i>3)
              {
                try
                {
                    
                    if(!lastLine.equals("") && MercatorUtil.getDistance(PraseGPGGAFlatFile(line).lat, PraseGPGGAFlatFile(line).lon, PraseGPGGAFlatFile(lastLine).lat, PraseGPGGAFlatFile(lastLine).lon)>8 )
                    {
                    geoWay.add(PraseGPGGAFlatFile(line));
                    lastLine = line;
                    }
                    else if (lastLine.equals(""))
                    {
                    geoWay.add(PraseGPGGAFlatFile(line));
                    lastLine = line; 
                        
                    }
                    
                    
                }catch(Exception ex)
                {                    
                    ex.printStackTrace();
                }  
                i=0;
              }
            }  
            
            
          
            
            
        } catch (Exception ex) {
            Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
               br.close();
            } catch (IOException ex) {
                Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }
}
