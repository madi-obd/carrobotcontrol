/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programcontroller;

import actuators.IActuatorServer;
import android.IAndroidServer;
import com.badlogic.gdx.math.Vector2;
import control.IAutoDataServer;
import control.ICarDynamicsControl;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import kpp.ChangeDriveServer;
import kpp.IChangeDriveServer;
import navigation.INavigationServer;
import navigation.IWayProcessor;
import robotautomaticcontrol.PickingHandler;
import robotautomaticcontrol.Renderer;
import robotautomaticcontrol.ViewFieldController;
import types.DataFromCar;
import types.DataFromControlAndroid;

/**
 *
 * @author Мади
 */
public class SystemManager {

	boolean inited = false;

    public boolean isInited() {
        return inited;
    }

    public void setInited(boolean inited) {
        this.inited = inited;
    }
	private static SystemManager instance;
        private PickingHandler pickingHandler;
	private ViewFieldController viewController;
	private Renderer renderer;
	private double wrap = 1;
	private double currentTime = 0;
	private File lastFile = null;
        INavigationServer navigationServer;
        IAutoDataServer autoDataCANServer;
        IAndroidServer androidServer;
        IActuatorServer actuatorServer;
        ICarDynamicsControl carDymamicsServer;
        IChangeDriveServer changeDriveServer;

    public IChangeDriveServer getChangeDriveServer() {
        return changeDriveServer;
    }

    public void setChangeDriveServer(IChangeDriveServer changeDriveServer) {
        this.changeDriveServer = changeDriveServer;
    }

	private long startTime;
	private long finishTime;
	
        private boolean isAutomaticDriveMode = false;
        
        private boolean isDrivingActive = false;
        
            IWayProcessor wayProcessor;

        public IWayProcessor getWayProcessor() {
            return wayProcessor;
        }

        public void setWayProcessor(IWayProcessor wayProcessor) {
            this.wayProcessor = wayProcessor;
        }

        public INavigationServer getNavigationServer() {
            return navigationServer;
        }

        public void setNavigationServer(INavigationServer navigationServer) {
            this.navigationServer = navigationServer;
        }

        public IAutoDataServer getAutoDataCANServer() {
            return autoDataCANServer;
        }

        public void setAutoDataCANServer(IAutoDataServer autoDataCANServer) {
            this.autoDataCANServer = autoDataCANServer;
        }

        public IAndroidServer getAndroidServer() {
            return androidServer;
        }

        public void setAndroidServer(IAndroidServer androidServer) {
            this.androidServer = androidServer;
        }

        public IActuatorServer getActuatorServer() {
            return actuatorServer;
        }

        public void setActuatorServer(IActuatorServer actuatorServer) {
            this.actuatorServer = actuatorServer;
        }

        public ICarDynamicsControl getCarDymamicsServer() {
            return carDymamicsServer;
        }

        public void setCarDymamicsServer(ICarDynamicsControl carDymamicsServer) {
            this.carDymamicsServer = carDymamicsServer;
        }
           

        public boolean isIsDrivingActive() {
            return isDrivingActive;
        }

        public void setIsDrivingActive(boolean isDrivingActive) {
            this.isDrivingActive = isDrivingActive;
        }

        public boolean isIsAutomaticDriveMode() {
            return isAutomaticDriveMode;
        }

        public void setIsAutomaticDriveMode(boolean isAutomaticDriveMode) {
            this.isAutomaticDriveMode = isAutomaticDriveMode;
        }
	
	private SystemManager(  )
	{
        this.bw = null;
		initManagers();
            
	}
	
	public static SystemManager getInstance()
	{
		if (instance == null) instance = new  SystemManager();
		return instance;
	}
	class MyThread extends Thread {
                double time = System.currentTimeMillis();
                public void run(){
                    while(true){
                    try {
                        Thread.sleep(50);                        
                    } catch (InterruptedException ex) {
                        Logger.getLogger(SystemManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                   update((System.currentTimeMillis()-time)/1000);                   
                   time = System.currentTimeMillis();
                    }
                }
              }
        
        MyThread threadUpdater = null;        
        BufferedWriter bw;
        FileWriter fw = null;
        Thread kppThread = null;
	public void startDrive()
	{
		//carDymamicsServer.setWay(new LinkedList<Vector2>( Renderer.getRenderer().points));            
            if(isAutomaticDriveMode){
               getCarDymamicsServer().setWay( getWayProcessor().GetWayNormilized());
            }
               threadUpdater = new MyThread();
               setCurrentTime(System.currentTimeMillis());
               
               kppThread=new Thread( (ChangeDriveServer)changeDriveServer);
               kppThread.setDaemon(true);
               kppThread.start();
               
            try {
                fw = new FileWriter("logpryamo" +String.valueOf(System.currentTimeMillis()) +".txt");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
	       bw = new BufferedWriter(fw);
                threadUpdater.start();		
	}
	
	public void stopDrive()
	{	    
                kppThread.stop();
                threadUpdater.stop();
                changeDriveServer.Stop();
		actuatorServer.writeData(-30, 0);
               
                try {
                if (bw != null)
			bw.close();
                if (fw != null)					
                        fw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }                
	}
	     
	public void pauseDrive()
	{
		
		
	}
	
	public void unPauseDrive()
	{
		
		
	}
	
	public void dispose()
	{
		 
		
	}
	
	protected void initManagers()
	{
		pickingHandler = new PickingHandler();
	}
	
	public void init()
	{
		
	}
	
	public void loadMap(File file)
	{	
		
	}
		
	public void finish()
	{
		
	}
	
	/*public void exportData()
	{
		if (ConditionManager.getInstance().getRunningState()!= States.FINISHED) return;
		ResultsExporter.getInstance().showFinishMessage();
	}*/
	/*
	public boolean updateInformation(StatisticsData data)
	{
		SceneObjectData sdata = new SceneObjectData(data);
		this.currentTime = data.getUniverseTime();
		NewSWTApp.getInstance().setInformation(sdata);
		if (!VisualController.getInstance().isSimulationRunning()) {

			resultStatistic = sdata;
			NewSWTApp.getInstance().callAskDialog("Would you like to save Statistic to file?");
			stop();
			return true;
		}
		return false;
	}*/
		
	public void update(double dt)
	{
		if(isDrivingActive)
                {
                    if(isAutomaticDriveMode)
                    {
                        navigationServer.UpdateCarXYAngleFromDynamics(autoDataCANServer.getSpeed()*1000/3600, actuatorServer.getDataSteering(), (System.currentTimeMillis()-currentTime)/1000);
                        //(double carX, double carY, double carAngle, double carSpeed, double distInFront)
                        carDymamicsServer.updatePosition(navigationServer.getCarX(), navigationServer.getCarY(),
                                                         navigationServer.getCarAngle(), autoDataCANServer.getSpeed(), (System.currentTimeMillis()-currentTime)/1000);
                        
                        //actuatorServer.writeData((int)carDymamicsServer.getAcceleration(), (int)carDymamicsServer.getSteeringWheel());
                        actuatorServer.writeData(0, (int)carDymamicsServer.getSteeringWheel());
                                                
                         try {                            
                            bw.write(String.valueOf(System.currentTimeMillis())+"|"+String.valueOf(autoDataCANServer.getSpeed())+"|"
                            +String.valueOf((int)carDymamicsServer.getSteeringWheel())+"|"+String.valueOf(androidServer.getDfa().getAcceleratioRate())+"|"
                            +navigationServer.getLatitude()+"|"+navigationServer.getLongitude());
                            bw.write(navigationServer.getGPRNC() + "\n");
                            
                        } catch (IOException ex) {
                           ex.printStackTrace();
                        }                        
                    }
                    else
                    {
                        DataFromControlAndroid androidData = androidServer.getDfa();
                        androidServer.setDfa(androidData);
                        
                        changeDriveServer.setCanDriveKPP(autoDataCANServer.getCurrentKPP());
                        changeDriveServer.setNewDriveKPP(androidData.getDriveKPP());
                        
                        
                       
                        DataFromCar carData = new DataFromCar();
                        
                        carData.setEngineRPM(autoDataCANServer.getRPM());
                        carData.setSpeed(autoDataCANServer.getSpeed());
                        androidServer.setDfc( carData);
                        actuatorServer.writeData(androidData.getAcceleratioRate(), androidData.getSteeringWheelRate());
                        
                        try {
                            
                            bw.write(String.valueOf(System.currentTimeMillis())+"|"+String.valueOf(autoDataCANServer.getSpeed())+"|"
                            +String.valueOf(androidServer.getDfa().getSteeringWheelRate())+"|"+String.valueOf(androidServer.getDfa().getAcceleratioRate())+"|"
                            +navigationServer.getLatitude()+"|"+navigationServer.getLongitude());
                            bw.write(navigationServer.getGPRNC() + "\n");
                            
                        } catch (IOException ex) {
                           ex.printStackTrace();
                        }
                    }                      
                    setCurrentTime(System.currentTimeMillis());
                }		
	}
	
	public PickingHandler getPickingHandler() {
		return pickingHandler;
	}
	
	public File getCurrentFile() {
		return lastFile;
	}
	
	protected void setCurrentTime(double t)
	{
		this.currentTime = t;
	}
	
        public ViewFieldController getViewController() {
		return viewController;
	}

	public Renderer getRenderer() {
		if (renderer == null) renderer = Renderer.createRenderer();
		return renderer;
	}
        
}