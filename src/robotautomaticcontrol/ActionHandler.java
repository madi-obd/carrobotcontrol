/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
 
import javax.swing.event.MouseInputListener;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_DEPTH_COMPONENT;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.glReadPixels;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import programcontroller.SystemManager;
import robotautomaticcontrol.forms.PointXY;

public class ActionHandler implements MouseMotionListener,MouseWheelListener,MouseInputListener {
 
	protected MouseInputHandler handler;
	public static int x;
	public static int y;
	
	protected int pressedX;
	protected int pressedY;
	
	protected boolean isPressed = false;
	protected enum ButtonType {
		Left,
		Right
	}
	protected ButtonType pressedButton;
	
	public ActionHandler(MouseInputHandler handler) {
		super();
		this.handler = handler;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (isPressed) {
			if (pressedButton == ButtonType.Left) {
				if(SystemManager.getInstance().getPickingHandler().getSelectedPoint()!=null)
                                {
                                    
                                    PointXY pt = SystemManager.getInstance().getPickingHandler().getSelectedPoint();
                                    float pintx = (float)pt.getBegin().x;
                                    float pinty = (float)pt.getBegin().y;
                                    Vector3f CurrentPos  =UnprojectMouse(e.getX(), e.getY());
                                    float dx = pintx-(unprojectedMousePosition.x - CurrentPos.x);
                                    float dy = pinty-(unprojectedMousePosition.y - CurrentPos.y);
                                    pt.getBegin().x = dx;
                                    pt.getBegin().y = dy;
                                    Renderer.getRenderer().makeDirty();
                                }
			} else {
				handler.mapMove(x - e.getX(), -y + e.getY());
                                mouseDraggedMap(x- e.getX(), y- e.getY());
			}
			
			x = e.getX();
			y = e.getY();
                        unprojectedMousePosition =UnprojectMouse(x, y);
		}
		//System.out.println("Mouse dragged");
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		 
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
        Vector3f unprojectedMousePosition=null;
        
        
        
    public static FloatBuffer winZ = BufferUtils.createFloatBuffer(1);    
    public static FloatBuffer modelview = BufferUtils.createFloatBuffer(16); //The model view for caching
 
    public static FloatBuffer projection = BufferUtils.createFloatBuffer(16); //The projection for caching
 
    public static IntBuffer viewport = BufferUtils.createIntBuffer(16); //The view port for caching
 
    private final static FloatBuffer position = FloatBuffer.allocate(3); //A temporary buffer
    private final static FloatBuffer position1 = FloatBuffer.allocate(3); //A temporary buffer
 
    private final static double[] tempVec = new double[3];
        
        //Generate the private variables before calling the below functions that rely on them
    public  void generateArrays() {
        modelview.clear();
        projection.clear();
        viewport.clear();
         
    }
            
    static public Vector3f getMouseOnPlaneZ(int mouseX, int mouseY)
    {
 
            float winX, winY;

            winX = (float)mouseX;
            winY = (float)mouseY;
            GLU.gluUnProject(winX, winY, 0.0f, modelview, projection, viewport, position);
            GLU.gluUnProject(winX, winY, 1.0f, modelview, projection, viewport, position1);
            float zeropoint, zeroperc;
            double posXt, posYt, posZt;
            posXt = position.get(0) - position1.get(0);
            posYt = position.get(1) - position1.get(1);
            posZt = position.get(2) - position1.get(2);
            if ((position.get(2) < 0.0 && position1.get(2) < 0.0) || (position.get(2) > 0.0 && position1.get(2) > 0.0))
            return null;
            zeropoint = 0.0f - (float)position.get(2);
            //Find the percentage that this point is between them
            zeroperc = (zeropoint / (float)posZt);
            Vector3f v = new Vector3f((float)position.get(0) + (float)(posXt * zeroperc),(float)position.get(1) + (float)(posYt * zeroperc),(float)position.get(2) + (float)(posZt * zeroperc));
            return v ;
        }

        public Vector3f UnprojectMouse(int x, int y)
        {

             float  newy =  viewport.get(3)-(float)y;
             float newx = (float )x;
         
             System.out.println(modelview.get(15));
      
//                projection.rewind();
//                modelview.rewind();
//                viewport.rewind();
//                tempBuffer.rewind();
             return getMouseOnPlaneZ((int)newx, (int)newy);

        }
                
         public Vector3f UnprojectCanvasCenter()
        {
            int x, y;
            x = Renderer.getRenderer().getCanvas().getHeight()/2;
            y = Renderer.getRenderer().getCanvas().getHeight()/2;
            float  newy =  viewport.get(3)-(float)y;
            float newx = (float )x;
         
            System.out.println(modelview.get(15));
      
            return getMouseOnPlaneZ((int)newx, (int)newy);
        }
                
	@Override
	public void mousePressed(MouseEvent e) {
		if (!isPressed) {
			isPressed = true;
			x = e.getX();
			y = e.getY();
                        unprojectedMousePosition =UnprojectMouse(x, y);
			pressedX = x;
			pressedY = y;
			this.pressedButton = determineButton(e.getButton());
			if (pressedButton == ButtonType.Left) {
                        
                            
                            SystemManager.getInstance().getPickingHandler().handlePick(e.getX(),e.getY());
                        }
		}
	}
	
	protected ButtonType determineButton(int b) {
		switch (b) {
		case MouseEvent.BUTTON1 : return ButtonType.Left;
		case MouseEvent.BUTTON3 : return ButtonType.Right;
		default : return ButtonType.Right;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (isPressed) {
			x = 0;
			y = 0;
                        
			isPressed = false;
			//handler.stopSpeedMove();
                        SystemManager.getInstance().getPickingHandler().unpick();
                        
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		handler.scaleChanged(e.getWheelRotation(),e.getClickCount());
                mouseWheelMovedMap(e);
	}
                
     ////////////////////////////////////////////////////////////////////////   
            //////////////////////////////////////////////////////////////////////// 
            //////////////////////////////////////////////////////////////////////// 
            //////////////////////////////////////////////////////////////////////// 
            //////////////////////////////////////////////////////////////////////// 
            //////////////////////////////////////////////////////////////////////// 
            ////////////////////////////////////////////////////////////////////////     ////////////////////////////////////////////////////////////////////////     //////////////////////////////////////////////////////////////////////// 
            //////////////////////////////////////////////////////////////////////// 
        
 
    public void mouseDraggedMap(int diffx, int diffy) 
    {
          Renderer.getRenderer().moveMap(diffx, diffy);
    }
             
    public void mouseWheelMovedMap(MouseWheelEvent e) {        
            Renderer.getRenderer().setZoom(Renderer.getRenderer().getZoom() - e.getWheelRotation(), e.getPoint());         
    }	
}
