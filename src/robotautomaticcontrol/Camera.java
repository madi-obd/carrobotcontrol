/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol;

 
import java.util.ArrayList;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.glScalef;
import org.lwjgl.util.vector.Vector3f;
import robotautomaticcontrol.forms.Vector3d;

public class Camera {

	protected static final float[] zoomfactor = {
		20f,1f,3f,7f,9f,14f,25f,50f,100f
	};
	
	protected Vector3d translation;
	protected float scale = -50f;
        float zoom = 1;
 
	protected boolean moved = true;
	
	public Camera(Vector3d translation)
	{
		this.translation = translation;		 
	}
	
	public Camera()
	{
		this.translation = new Vector3d(0,0);		 
	}
	
	public void moveCamera(Vector3f vector)
	{
		
		this.translation.x += vector.x;
		this.translation.y += vector.y;
		//this.translation.z += vector.z;
		Renderer.getRenderer().makeDirty();
		 
		moved = true;
	}
	
        
        public void setCamera(Vector3d vector)
	{
		
		this.translation.x = vector.x;
		this.translation.y = vector.y;
		//this.translation.z = vector.z;
		Renderer.getRenderer().makeDirty();
		 
		moved = true;
	}
        
	public void moveAway(float sign, int amount )
	{
		/*if (Math.abs(dz) < 0.00001f) return;
		if (dz < 0 ) this.scale /= dz; else {
			this.scale *= dz;
		}*/
		if (amount < 0) amount = 3;
		if (amount > zoomfactor.length -1) amount = zoomfactor.length -1;
		this.scale += sign*zoomfactor[amount];
                zoom = zoomfactor[amount];
		if (scale > -1.0f) scale = -1.0f;
		Renderer.getRenderer().getCanvas().repaint();
		moved = true;
	}
	
	public void moveCamera(float x, float y , float z)
	{
		//this.translation.x += 0.5f*x/scale;
		//this.translation.y += 0.5f*y/scale;
		this.translation.x += 0.5f*x/0.02f;
		this.translation.y -= 0.5f*y/0.02f;
		//this.translation.z += z;
		Renderer.getRenderer().makeDirty();
		 
		moved = true;
	}
	
	public void update()
	{
            
		GL11.glLoadIdentity();
                glScalef(1.0f, -1.0f, 1.0f);
		//GL11.glTranslated(-translation.x, -translation.y, scale);
                GL11.glTranslated(0, 0, scale);
	}

	public Vector3d getTranslation() {
		return translation;
	}

	public void setTranslation(Vector3d translation) {
		this.translation = translation;
		Renderer.getRenderer().makeDirty();		 
	}
	
	public float getScale()
	{
		return this.scale;
	}
        
	public float getFactor()
	{		 
		return zoom;
	}	 
}