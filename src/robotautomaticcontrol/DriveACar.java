/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol;

import control.CarDynamicsController;
import helpers.MercatorUtil;
import helpers.VectorGeo2;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import navigation.WayProcessor;
import org.lwjgl.util.vector.Vector2f;
import org.openstreetmap.gui.jmapviewer.OsmMercator;
import programcontroller.SystemManager;
import robotautomaticcontrol.forms.PointXY;
import robotautomaticcontrol.forms.Vector3double;
import static navigation.WayProcessor.PraseGPGGAFlatFile;
import trafficobjects.Car;

/**
 *
 * @author Admin
 */
public class DriveACar {
    public static void main(String[] args) {
        
        CarDynamicsController carDynamicsController = new CarDynamicsController();
        WayProcessor wayProcessor = new WayProcessor();
        wayProcessor.LoadWayNormalizedFromFileRealGPS("C:\\Users\\Admin\\Documents\\NetBeansProjects\\RobotMainControl\\logpryamo1493210433027.txt");
        carDynamicsController.setWay(wayProcessor.GetWayNormilized());
         
        
        FileInputStream fis = null;
        BufferedReader br = null;
         
        
        try {
              VectorGeo2 last_vector =null;
            
            fis = new FileInputStream("C:\\Users\\Admin\\Documents\\NetBeansProjects\\RobotMainControl\\logpryamo1493209044124.txt");
           
           // fis = new FileInputStream("C:\\Users\\Admin\\Documents\\NetBeansProjects\\RobotMainControl\\logpryamo1493209044124.txt");
            //Construct BufferedReader from InputStreamReader
            br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            boolean flug = true;
            int i=0;
            double angle =0;
            while ((line = br.readLine()) != null) 
            { 
              i++;
            // if(i>50)
            //  {
                  i=0;
                  
//                  if(flug)
//                  {
//                    flug = false;
//                    continue;
//                  }
                  
                  
                try{
                     String strline=line;

               String[] parts = strline.split(Pattern.quote("|"));
                String shirota = parts[4]; // 3723.465874
                String p3 = parts[3]; // N
                String dolgota = parts[5]; // 12202.26954
                String p5 = parts[5]; // W
                
                
                Car car = new Car();
                
                car.setLat(Double.parseDouble(shirota));
                car.setLon(Double.parseDouble(dolgota));
                
                if(last_vector!=null)
                {
                     if(MercatorUtil.getDistance(last_vector.lat, last_vector.lon, Double.parseDouble(shirota), Double.parseDouble(dolgota))>10)
                        {
                angle = MercatorUtil.bearing(last_vector.lat, last_vector.lon, car.lat, car.lon);   
                last_vector = new VectorGeo2(Double.parseDouble(shirota), Double.parseDouble(dolgota));
                        }
                VectorGeo2 geoVec = wayProcessor.getXYCarPositionNormilized(car);
                carDynamicsController.updatePosition(geoVec.lat, geoVec.lon, angle
                                                         , 1, 0);
                
                
                System.out.println(carDynamicsController.getSteeringWheel());
                
                }else{
                
                
                last_vector = new VectorGeo2(Double.parseDouble(shirota), Double.parseDouble(dolgota));
                }
                }catch(Exception ex)
                {                    
                    ex.printStackTrace();
                }  
                
             // }
            }   
        } catch (Exception ex) {
            Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
               br.close();
            } catch (IOException ex) {
                Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
         
        System.out.println("Hello World!");
    }
}
