/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol;
import java.awt.Color;
/**
 *
 * @author Мади
 */
public interface IControlSystemWindow {
    
    void AddControlPortToUI(String port);
    void AddCanPortToUI(String port);
    void AddNavigationPortToUI(String port);
    void SetConsoleTextColor(Color color);
    void LogTextToUi(String text);
    void MakeAutoscrol();
    void SetRPM(int rpm);
    void SetSpeed(int speed);
    void SetLatitude(double latitude);
    void SetLongitude(double longitude);
}
