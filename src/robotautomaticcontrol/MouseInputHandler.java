/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol;

import com.badlogic.gdx.math.Vector2;
import java.util.Timer;
import java.util.TimerTask;
import programcontroller.SystemManager;

public class MouseInputHandler {
	
	protected static float moveFactor = 0.005f;
	protected static float speedFactor = 0.0000007f;
	protected Camera camera = null;
	protected Timer timer;
	protected Vector2 speed;
	
	public MouseInputHandler()
	{
		this.camera =  SystemManager.getInstance().getRenderer().getCamera();
	}
	
	/**
	 * Called when position of the camera must be changed;
	 * @param dx - delta in x coordinate in units;
	 * @param dy - delta in y coordinates in units;
	 */
	public void mapMove(float dx, float dy)
	{
		camera.moveCamera(dx*moveFactor, dy*moveFactor, 0);
                Renderer.getRenderer().getCanvas().repaint();
	}
	
	/**
	 * Called when height of the camera above scene must be changed.
	 * Usually this method handle mouse wheel action
	 * if dw > 0 - height increase. otherwise height decrease;
	 * @param dw - value of change;
	 */
	public void scaleChanged(int dw, int clicks)
	{
		if (dw == 0) return;
		float sign = Math.signum(-dw);
		camera.moveAway(sign,clicks);
                Renderer.getRenderer().getCanvas().repaint();
	}
	
	/*public void startSpeedMove() {
		if (this.timer != null) timer.cancel();
		this.timer = new Timer();
		speed = new Vector2();
		this.timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				camera.moveCamera(speed.x, speed.y, 0);
			}
		}, 100, 10);
	}
	
	public void stopSpeedMove() {
		if (this.timer == null) return;
		this.timer.cancel();
	}
	 
	 public void continueingMapMove(int startX, int startY , int x , int y){
		speed.x =  -(x - startX);
		speed.y =  -(y - startY);
		float ls = speed.len();
		speed.nor();
		speed.x*=(ls*speedFactor);
                speed.y*=(ls*speedFactor);
	}
        */
}