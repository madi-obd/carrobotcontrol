/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import programcontroller.SystemManager;
import robotautomaticcontrol.Renderer.RenderType;
import robotautomaticcontrol.forms.PointXY;
import static org.lwjgl.opengl.GL11.*; 
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Мади
 */
public class PickingHandler{
        private static HashMap<Long , Object> objectsForPicking;
	private static int lastId = 1;
	
	private boolean picked = false; 
	private int mouseX = 0;
	private int mouseY = 0;
	private  PointXY selected_point = null;
        private Vector3f unprojectedPointPosition = null;
	
	public void handlePick(int mouseX, int mouseY)
	{
		this.picked = true;
		this.mouseX = mouseX;
		this.mouseY = mouseY;
		Renderer.getRenderer().getCanvas().repaint();
	}
        public void unpick()
        {            
            System.out.println("Unpick!");
            selected_point=null;
            picked=false;
            unprojectedPointPosition = null;
            Renderer.getRenderer().getCanvas().repaint();
        }
	
	public void processPicking(int canvasHeight)
	{
            
	if (!picked) return; else picked = false;
                         
	IntBuffer selectBuf = BufferUtils.createIntBuffer(512);
        int hits;
        IntBuffer viewport = BufferUtils.createIntBuffer(16);
        Renderer renderer = Renderer.getRenderer();

        glGetInteger(GL11.GL_VIEWPORT, viewport);
        
//
        GL11.glSelectBuffer(selectBuf);
        glRenderMode(GL11.GL_SELECT);
       
//
        glInitNames();
        glPushName(0);
////
        
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
//        
        GLU.gluPickMatrix(mouseX, (canvasHeight - mouseY), 5.0f, 5.0f, viewport);
        GLU.gluPerspective(60.0f, 1, 0.0f, 1000.0f);
////        
        objectsForPicking = new HashMap<Long, Object>();
        lastId = 1;
//        
        //glMatrixMode(GL_MODELVIEW);
        
///*        glPushName(1);
//        GL11.glTranslatef(0,0,-50f);
//        glRectf(-1000, -1000, 1000, 1000);
//        glPopName();*/
        glMatrixMode(GL_MODELVIEW);
        //SystemManager.getInstance().getRenderer().getCamera().update();
        
        renderer.render(RenderType.Picking, SystemManager.getInstance().getRenderer().getCamera().translation);

        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
            try {
                Renderer.getRenderer().getCanvas().swapBuffers();
                glFlush();
            } catch (LWJGLException ex) {
                Logger.getLogger(PickingHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
               
        hits = GL11.glRenderMode(GL11.GL_RENDER);
        processHits(hits, selectBuf);
        glMatrixMode(GL_MODELVIEW);
         try {
                Renderer.getRenderer().getCanvas().swapBuffers();
            } catch (LWJGLException ex) {
                Logger.getLogger(PickingHandler.class.getName()).log(Level.SEVERE, null, ex);
            }        
	}
	
	public static int addObject(Object o)
	{
		lastId++;
		objectsForPicking.put(new Long(lastId), o);
		return lastId;
	}
	
	private void processHits(int hits, IntBuffer sb)
	{
		Long name ;
		Object object = null;
		//System.out.println("Hits : " + hits);
		for (int j = 0; j < hits; j++) { 
			int names = sb.get();
			sb.get();
			sb.get();
			for (int i = 0; i < names; i++) {
				name = new Long(sb.get());
				object = objectsForPicking.get(name);
				//System.out.println("Name : "+ name + " Object :"+object);
				this.handleResult(object);
			} 
		}                        		
	}
	
	protected void handleResult(Object o)
	{
		if (o instanceof PointXY) {
			PointXY point = (PointXY) o;
			this.selected_point = point;                        					
			return;
		}				 
	}
	
	public boolean needProcess() {
		return picked;
	}

	public PointXY getSelectedPoint() {
		return selected_point;
	}
}