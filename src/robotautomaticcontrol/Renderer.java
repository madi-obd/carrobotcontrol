/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol;

import com.badlogic.gdx.math.Vector3;
import helpers.MercatorUtil;
import helpers.VectorGeo2;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.AWTGLCanvas;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;
import robotautomaticcontrol.forms.IRenderable;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glColor4f;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import navigation.SplineLine;
import org.lwjgl.BufferUtils;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.AWTGLCanvas;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.glColor3f;
import org.lwjgl.util.Color;
import org.lwjgl.util.Point;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MemoryTileCache;
import org.openstreetmap.gui.jmapviewer.OsmMercator;
import org.openstreetmap.gui.jmapviewer.Tile;
import org.openstreetmap.gui.jmapviewer.TileController;
import org.openstreetmap.gui.jmapviewer.interfaces.TileCache;
import org.openstreetmap.gui.jmapviewer.interfaces.TileLoaderListener;
import org.openstreetmap.gui.jmapviewer.interfaces.TileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.OsmTileSource;
import programcontroller.SystemManager;
import robotautomaticcontrol.forms.CarForm;
import robotautomaticcontrol.forms.PointXY;
import robotautomaticcontrol.forms.SegmentXY;
import robotautomaticcontrol.forms.Vector3d;
import robotautomaticcontrol.visualizer.Texture;
import robotautomaticcontrol.visualizer.TextureLoader;
import static robotautomaticcontrol.visualizer.TextureLoader.loadImage;

public class Renderer implements IRenderable, TileLoaderListener {

    public static enum RenderType {
        None,
        Picking
    }
    /**
     * Vectors for clock-wise tile painting
     */
    TextureLoader tl;
    protected static final java.awt.Point[] move = {new java.awt.Point(1, 0), new java.awt.Point(0, 1), new java.awt.Point(-1, 0), new java.awt.Point(0, -1)};

    public static final long MAX_UPDATE_TIME = 20;
    public Camera camera = null;
    protected static Renderer renderer;
    public ArrayList<SegmentXY> segments;
    public ArrayList<PointXY> points;
    
    
    public ArrayList<PointXY> pointsFactic;

    private CarForm carForm = null;

    public FrustrumState frustrumState = FrustrumState.InView;
    public Point viewport;
    private LWJGLCanvas canvas;
    private long startTime = 0;

    protected float lineWidth = 1f;

    protected boolean firstAfterInit = true;

    /**
     * Moves the visible map pane.
     *
     * @param x horizontal movement in pixel.
     * @param y vertical movement in pixel
     */
    public void moveMap(int x, int y) {
        center.x += x;
        center.y += y;

    }

    private Renderer() {
        super();
        tl = new TextureLoader();
        camera = new Camera();
        this.startTime = System.currentTimeMillis();

        segments = new ArrayList<SegmentXY>();
        SegmentXY seg = new SegmentXY(new Vector3d(30.0, 30.0), new Vector3d(0.0, 0.0), new Color(255, 0, 0));
        segments.add(seg);
        points = new ArrayList<PointXY>();
//        PointXY pt = new PointXY(new Vector2f(0.0f, 0.0f), new Color(255, 0, 0), 10);
//        points.add(pt);
//
        TileCache tileCache = new MemoryTileCache();
//
        tileSource = new OsmTileSource.Mapnik();
        tileController = new TileController(tileSource, tileCache, this);
//        pt = new PointXY(new Vector2f(10.0f, 0.0f), new Color(255, 0, 0), 10);
//        points.add(pt);
//        pt = new PointXY(new Vector2f(20.0f, 0.0f), new Color(255, 0, 0), 10);
//        points.add(pt);
//        pt = new PointXY(new Vector2f(30.0f, 0.0f), new Color(255, 0, 0), 10);
//        points.add(pt);
//        pt = new PointXY(new Vector2f(0.0f, 10.0f), new Color(255, 0, 0), 10);
//        points.add(pt);
//        pt = new PointXY(new Vector2f(0.0f, 10.0f), new Color(255, 0, 0), 10);
//        points.add(pt);

    }

    public static Renderer getRenderer() {
        if (renderer == null) {
            renderer = new Renderer();
        }
        return renderer;
    }

    public void init() {
        Vector2f af = SystemManager.getInstance().getViewController().getFocusPoint();
        camera.setTranslation(new Vector3d(af.x, af.y));
        this.makeDirty();
        this.firstAfterInit = true;

    }

    public static Renderer createRenderer() {
        if (renderer != null) {
//            LWJGLCanvas c = renderer.canvas;
//            renderer = new Renderer();
//            renderer.canvas = c;
        } else {
            renderer = new Renderer();
        }

        return renderer;
    }

    protected void texturedVertex(double tx, double ty,
            double x, double y) {
        GL11.glTexCoord2d(tx, ty);
        GL11.glVertex2d(x, y);
    }

    public void paintTile( Vector3d translation, Tile tile, double posx1, double posy1, double posx2, double posy2) {

//            try 
//            {
//                // retrieve image
//                BufferedImage bi = tile.getImage();
//                File outputfile;
//                outputfile = new File("C:\\saved.png");
//                ImageIO.write(bi, "png", outputfile);
//            } catch (IOException e) 
//            {
//                e.printStackTrace();
//            }
//            try 
//            {                
//                ByteArrayOutputStream os = new ByteArrayOutputStream();
//                ImageIO.write(tile.getImage(), "png", os);
//                InputStream is = new ByteArrayInputStream(os.toByteArray());
//                texture = tl.getTextureFromStream("hi", is, true);
//            } catch (IOException ex) 
//            {
//                ex.printStackTrace();
//            }
        Texture t = tile.getTexture(tl);
        if (t != null) {
            t.bind();
        }
        glColor3f(1.0f, 1.0f, 1.0f);

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glBegin(GL11.GL_QUADS);
        //GL11.glColor3f(color.r, color.g, color.b);
        this.texturedVertex(0, 0,   posx1-translation.x,   posy1-translation.y);
        this.texturedVertex(1, 0,   posx2-translation.x,   posy1-translation.y);
        this.texturedVertex(1, 1,   posx2-translation.x,   posy2-translation.y);
        this.texturedVertex(0, 1,   posx1-translation.x,   posy2-translation.y);
        GL11.glEnd();
        GL11.glDisable(GL11.GL_TEXTURE_2D);

    }

    public void renderTiles(Vector3d translation) {
        int maxzoom = 17;
        center = new java.awt.Point((int) camera.translation.x, (int) camera.translation.y);

        ///Получили точку щентра экрана
        double lon = MercatorUtil.XToLon(center.x, 1);
        double lat = MercatorUtil.YToLat(center.y, 1);

        //Определяем тайлы для нее
        int ytile_pos = OsmMercator.LatToY(lat, 17);
        int xtile_pos = OsmMercator.LonToX(lon, 17);

        int iMove = 0;
        int tilesize = tileSource.getTileSize();
//            int tilex = center.x / tilesize;
//            int tiley = center.y / tilesize;
//            int off_x = (center.x % tilesize);
//            int off_y = (center.y % tilesize);

        int tilex = xtile_pos / tilesize;
        int tiley = ytile_pos / tilesize;
        int off_x = (xtile_pos % tilesize);
        int off_y = (ytile_pos % tilesize);

        int w2 = canvas.getWidth() / 2;
        int h2 = canvas.getHeight() / 2;
        int posx = w2 - off_x;
        int posy = h2 - off_y;

        int diff_left = off_x;
        int diff_right = tilesize - off_x;
        int diff_top = off_y;
        int diff_bottom = tilesize - off_y;

        boolean start_left = diff_left < diff_right;
        boolean start_top = diff_top < diff_bottom;

        if (start_top) {
            if (start_left) {
                iMove = 2;
            } else {
                iMove = 3;
            }
        } else if (start_left) {
            iMove = 1;
        } else {
            iMove = 0;
        } // calculate the visibility borders    
        // calculate the visibility borders
        int x_min = -tilesize;
        int y_min = -tilesize;
        int x_max = canvas.getWidth();
        int y_max = canvas.getHeight();

        // paint the tiles in a spiral, starting from center of the map
        boolean painted = true;
        int x = 0;

        for (int i = -2; i < 3; i++) {

            for (int j = -2; j < 3; j++) {

                int mini_tile_x = tilex + i;
                int mini_tile_y = tiley + j;

                int pos_mini_tile_y = tiley + j;
                // tile is visible

                Tile tile = tileController.getTile(mini_tile_x, mini_tile_y, 17);

                if (tile != null) {

                    painted = true;

                    double real_tile_x1 = MercatorUtil.LonToX(OsmMercator.XToLon(mini_tile_x * 256, 17), iMove);
                    double real_tile_y1 = MercatorUtil.LatToY(OsmMercator.YToLat(pos_mini_tile_y * 256, 17), iMove);
                    double real_tile_x2 = MercatorUtil.LonToX(OsmMercator.XToLon(mini_tile_x * 256 + 256, 17), iMove);
                    double real_tile_y2 = MercatorUtil.LatToY(OsmMercator.YToLat(pos_mini_tile_y * 256 + 256, 17), iMove);

                    paintTile(translation, tile, real_tile_x1, real_tile_y2, real_tile_x2, real_tile_y1);
//                                System.out.println("tilex = "+ String.valueOf(tilex) + " tile y = " + String.valueOf(tiley));
//                                
//                                System.out.println(" Center y = "+String.valueOf( center.x) +" Center y = " +String.valueOf( center.y));
//                                System.out.println(" Center y1 = "+String.valueOf( real_tile_x1) +" Center y1 = " +String.valueOf( real_tile_y1));
//                                System.out.println(" Center y2 = "+String.valueOf( real_tile_x2) +" Center y2 = " +String.valueOf( real_tile_y2));

                }
            }
        }
    }

    @Override
    public void render(RenderType mode, Vector3d translation) {
        if (SystemManager.getInstance().isInited() == false) {

            return;
        }
        long time = System.currentTimeMillis();
        glClear(GL_COLOR_BUFFER_BIT);
        GL11.glLineWidth(camera.getScale() * lineWidth);

        //initFonts();		
        GL11.glPushMatrix();
        GL11.glPopMatrix();
      //  GL11.glLoadIdentity();
        renderTiles(translation);

//         GL11.glPushMatrix();
//        if(points!=null&&points.size()>0){
//        GL11.glTranslatef( (float)points.get(0).getBegin().x, (float)points.get(0).getBegin().y, 0);
//        for (SegmentXY segXY : segments) {
//            segXY.render(mode);
//        }
//        GL11.glPopMatrix();
        for (SegmentXY segXY : segments) {
        //    segXY.render(mode, translation);
        }

        //fontManager.drawString("Ololo", 1, 1, ru.cos.sim.visualizer.color.Color.red);
        
        	GL11.glColor3f(1.0f, 0.0f, 0.0f);
        for (PointXY pointXY : points) {
            
              
              pointXY.render(mode, translation);
        }
        	GL11.glColor3f(1.0f, 0.0f, 1.0f);
//        for (PointXY pointXY: pointsFactic)
//        {
//            
//               pointXY.render(mode, translation);
//        }
        
        SplineLine sl = new SplineLine();                       
        for(PointXY pts : points){
            Vector3 vk = new Vector3((float)(pts.getBegin().x-translation.x),(float)(pts.getBegin().y-translation.y), 0.0f);
            
            
            float a = (float)(pts.getBegin().x-translation.x);
            float b = (float)(pts.getBegin().y-translation.y);
            String str = ""+a;
           // System.out.println("x="+str);
           // System.out.println("y="+vk.y);
            //if(str.equals("NaN"))
            //{
            //    str = ""+vk.x;
                
            //}
            sl.controlPointsList.add(new Vector3(a,b, 0.0f));
        }       
        List<Vector3> vecInterpos = sl.GetcontrolPointsInterpolated();
        
        
         for(int i=0; i<vecInterpos.size();i++){
             
             Vector3 begin = vecInterpos.get(i);
             GL11.glColor3f(1.0f, 0.0f, 0.0f);
		GL11.glBegin(GL11.GL_LINE_STRIP);
		
                double dxa = (begin.x-0.1);
                double dxb = (begin.x +0.1);
                
                GL11.glVertex2d( dxa,  begin.y );
		GL11.glVertex2d( dxb,  begin.y );
                
		GL11.glEnd();
                GL11.glBegin(GL11.GL_LINE_STRIP);
		
                
                double dya =  (begin.y-0.1 );
                double dyb =  (begin.y+0.1);
                GL11.glVertex2d( begin.x , dya);
		GL11.glVertex2d( begin.x , dyb);
                
		GL11.glEnd();
                if(i<vecInterpos.size()-1)
                {
                    
                    Vector3 end = vecInterpos.get(i+1);
                    Vector3 end1 = new Vector3(end.x, end.y, end.z);
                    Vector3 beginNew = new Vector3(begin.x, begin.y, begin.z);
                    Vector3 dir = end1.sub(begin);
                    Vector3 newEnd = dir.scl(10.0f);
                    beginNew.add(newEnd);
                    
                    GL11.glBegin(GL11.GL_LINE_STRIP);
		
                
               
                GL11.glVertex2d( begin.x , begin.y);
		GL11.glVertex2d(beginNew.x, beginNew.y);
                
		GL11.glEnd();
                    
                    
                }
                i++;
        }  
       
       
       
       
        

        if (carForm == null) {
            carForm = new CarForm(SystemManager.getInstance().getNavigationServer().getCar());
        } else {
            carForm.render(mode, translation);
            
        }
        GL11.glPopMatrix();

        try {
            this.canvas.swapBuffers();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        if (time - this.startTime > MAX_UPDATE_TIME) {
            this.canvas.repaint();
        }
        needUpdate = false;
    }

    protected TileController tileController;

    protected java.awt.Point center;

    private TileSource tileSource;
    /**
     * Current zoom level
     */
    protected int zoom = 0;

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom, java.awt.Point mapPoint) {
        if (zoom > tileController.getTileSource().getMaxZoom() || zoom < tileController.getTileSource().getMinZoom()
                || zoom == this.zoom) {
            return;
        }
        Coordinate zoomPos = getPosition(mapPoint);
        tileController.cancelOutstandingJobs(); // Clearing outstanding load
        // requests
        this.zoom = zoom;
    }

    /**
     * Creates a standard {@link JMapViewer} instance that can be controlled via
     * mouse: hold right mouse button for moving, double click left mouse button
     * or use mouse wheel for zooming. Loaded tiles are stored the
     * {@link MemoryTileCache} and the tile loader uses 4 parallel threads for
     * retrieving the tiles.
     */
    /**
     * Calculates the latitude/longitude coordinate of the center of the
     * currently displayed map area.
     *
     * @return latitude / longitude
     */
    public Coordinate getPosition() {
        double lon = OsmMercator.XToLon(center.x, zoom);
        double lat = OsmMercator.YToLat(center.y, zoom);
        return new Coordinate(lat, lon);
    }

    /**
     * Converts the relative pixel coordinate (regarding the top left corner of
     * the displayed map) into a latitude / longitude coordinate
     *
     * @param mapPoint relative pixel coordinate regarding the top left corner
     * of the displayed map
     * @return latitude / longitude
     */
    public Coordinate getPosition(java.awt.Point mapPoint) {
        return getPosition(mapPoint.x, mapPoint.y);
    }

    /**
     * Converts the relative pixel coordinate (regarding the top left corner of
     * the displayed map) into a latitude / longitude coordinate
     *
     * @param mapPointX
     * @param mapPointY
     * @return
     */
    public Coordinate getPosition(int mapPointX, int mapPointY) {
        int x = center.x + mapPointX - canvas.getWidth() / 2;
        int y = center.y + mapPointY - canvas.getHeight() / 2;
        double lon = OsmMercator.XToLon(x, zoom);
        double lat = OsmMercator.YToLat(y, zoom);
        return new Coordinate(lat, lon);
    }

    /**
     * Calculates the position on the map of a given coordinate
     *
     * @param lat
     * @param lon
     * @param checkOutside
     * @return point on the map or <code>null</code> if the point is not visible
     * and checkOutside set to <code>true</code>
     */
    public java.awt.Point getMapPosition(double lat, double lon, boolean checkOutside) {
        int x = OsmMercator.LonToX(lon, zoom);
        int y = OsmMercator.LatToY(lat, zoom);
        x -= center.x - canvas.getWidth() / 2;
        y -= center.y - canvas.getHeight() / 2;
        if (checkOutside) {
            if (x < 0 || y < 0 || x > canvas.getWidth() || y > canvas.getHeight()) {
                return null;
            }
        }
        return new java.awt.Point(x, y);
    }

    /**
     * Calculates the position on the map of a given coordinate
     *
     * @param lat
     * @param lon
     * @return point on the map or <code>null</code> if the point is not visible
     */
    public java.awt.Point getMapPosition(double lat, double lon) {
        return getMapPosition(lat, lon, true);
    }

    boolean needUpdate = false;

    @Override
    public void tileLoadingFinished(Tile tile, boolean success) {
        needUpdate = true;
        canvas.repaint();

    }

    @Override
    public TileCache getTileCache() {
        return tileController.getTileCache();
    }

    /**
     * Calculates the position on the map of a given coordinate
     *
     * @param coord
     * @return point on the map or <code>null</code> if the point is not visible
     */
    public java.awt.Point getMapPosition(Coordinate coord) {
        if (coord != null) {
            return getMapPosition(coord.getLat(), coord.getLon());
        } else {
            return null;
        }
    }

    /**
     * Calculates the position on the map of a given coordinate
     *
     * @param coord
     * @return point on the map or <code>null</code> if the point is not visible
     * and checkOutside set to <code>true</code>
     */
    public java.awt.Point getMapPosition(Coordinate coord, boolean checkOutside) {
        if (coord != null) {
            return getMapPosition(coord.getLat(), coord.getLon(), checkOutside);
        } else {
            return null;
        }
    }

    public Camera getCamera() {
        return camera;
    }

    public float getLineWidth() {
        return lineWidth * camera.getScale();
    }

    public int getViewportX() {
        return this.viewport.getX();
    }

    public int getViewportY() {
        return this.viewport.getY();
    }

    public void setVewport(int x, int y) {
        this.viewport = new Point(x, y);
    }

    public void makeDirty() {
        this.startTime = System.currentTimeMillis();
        this.canvas.repaint();

    }

    public AWTGLCanvas getCanvas() {
        return canvas;
    }

    public void setCanvas(LWJGLCanvas canvas) {
        this.canvas = canvas;
    }

}
