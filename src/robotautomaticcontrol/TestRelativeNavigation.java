///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package robotautomaticcontrol;
//
//import com.badlogic.gdx.math.Vector2;
//import control.CarDynamicsController;
//import helpers.MercatorUtil;
//import helpers.VectorGeo2;
//import java.io.BufferedReader;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.LinkedList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.regex.Pattern;
//import navigation.WayProcessor;
//import static navigation.WayProcessor.PraseGPGGAFlatFile;
//import trafficobjects.Car;
//
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package control;
//
//import java.util.LinkedList;
//import com.badlogic.gdx.math.Vector2;
//import com.badlogic.gdx.math.Quaternion;
//import org.neuroph.core.NeuralNetwork;
//
///**
// *
// * @author Admin
// */
//public class CarDynamicsController implements ICarDynamicsControl {
//
//    double acceleration = 0;
//    double steeringWheel = 0;
//    double carX = 0;
//    double carY = 0;
//    double carAngle = 0;
//    int currentWayPoint = 1;
//    NeuralNetwork neuralNetwork = null;
//
//    LinkedList<Vector2> way = null;
//
//    public CarDynamicsController() {
//        //  neuralNetwork = NeuralNetwork.createFromFile("C:\\Users\\Admin\\Documents\\NetBeansProjects\\CarController\\Neural Networks\\NewNeuralNetwork1.nnet");
//
//    }
//
//    @Override
//    public double getAcceleration() {
//        return acceleration;
//    }
//
//    @Override
//    public double getSteeringWheel() {
//        return steeringWheel;
//    }
//
//    @Override
//    public void updatePosition(double carX, double carY, double carAngle, double carSpeed, double distInFront) {
//
//        this.carX = carX;
//        this.carY = carY;
//        
//        if(currentWayPoint<3)
//        {
//            
//            carAngle = GetNextWayAngle(way.get(currentWayPoint-1), way.get(currentWayPoint));
//        }
//        this.carAngle = carAngle;
//         Vector2 gpsXYPosition = new Vector2((float) carX, (float) carY);
//         Vector2 distVector = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//        if (distVector.sub(gpsXYPosition).len() < 4.5) {
//            currentWayPoint++;
//            if (currentWayPoint == way.size()) {
//                currentWayPoint = 0;
//            }
//        }
//
//        int nextWaypoint = currentWayPoint + 1;
//        if (nextWaypoint == way.size()) {
//            nextWaypoint = 0;
//        }
//        int prevWayPoint = currentWayPoint - 1;
//        if (prevWayPoint == -1) {
//            prevWayPoint = way.size() - 1;
//        }
//
//        double nextWayAngle = GetNextWayAngle(way.get(prevWayPoint), way.get(currentWayPoint));
//        nextWayAngle = nextWayAngle;
//        
//        
//        
//        double angleDelta = nextWayAngle - carAngle;
//
//        if (angleDelta < -180) {
//            angleDelta = angleDelta + 2 * 180;
//        } else if (angleDelta > 180) {
//            angleDelta = angleDelta - 2 * 180;
//        }
//        double psy = angleDelta;
//
//       
//        
//        Vector2 curentCopy = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//        double dist_error = GetDelta(way.get(prevWayPoint), curentCopy.sub(way.get(prevWayPoint)), gpsXYPosition).sub(gpsXYPosition).len();
//        Vector2 prev_a = way.get(prevWayPoint);
//        Vector2 current_b = way.get(currentWayPoint);
//        Vector2 my_c = gpsXYPosition;
//
//        if (((current_b.x - prev_a.x) * (my_c.y - prev_a.y) - (current_b.y - prev_a.y) * (my_c.x - prev_a.x)) < 0) {
//            dist_error = -dist_error;
//            psy = psy;
//        }
//        
//        System.out.println("car angle = " + carAngle+ " nextwayangle =  "+ nextWayAngle+ " dist error = " + dist_error);
//        //var a = psy - Mathf.Rad2Deg * Mathf.Atan2(0.06 * dist_error  ,rigidbody.velocity.magnitude);
//        if(carSpeed<1)
//        {
//            
//            carSpeed=1;
//        }
//        double wheelAngle = psy + Math.toDegrees(Math.atan2(0.01 * dist_error, carSpeed));
//
//        if (wheelAngle > 33) {
//            wheelAngle = 33;
//        }
//        if (wheelAngle < -33) {
//            wheelAngle = -33;
//        }
//
//        this.steeringWheel = wheelAngle*14;
//         this.acceleration=0;      
//    }
//private static double GetNextWayAngle(Vector2 prevPoint, Vector2 nextPoint) {
//        double deltaY = prevPoint.x - nextPoint.x;
//        double deltaX = prevPoint.y - nextPoint.y;
//        
//        
//        double _angle = Math.toDegrees(Math.atan2(nextPoint.x-prevPoint.x, nextPoint.y-prevPoint.y));
//        if(_angle < 0 ){
//         return  360+_angle ;
//        }
//        else
//        {
//            
//            return  _angle;
//        }
//    }
//
//    private static Vector2  GetDelta(Vector2 origin, Vector2 direction, Vector2 point) {
//        
//        Vector2 direction1 = new Vector2(direction.x, direction.y);
//        Vector2 point1 = new Vector2(point.x, point.y);
//        direction1.nor();
//        
//        Vector2 v = point1.sub(origin);
//        float d = Vector2.dot(v.x, v.y, direction1.x, direction1.y);
//        Vector2 returnVecor = new Vector2(origin.x, origin.y);
//        return returnVecor.add(direction1.x * d, direction1.y * d);
//    }
//
//    @Override
//    public void setWay(LinkedList<Vector2> way) {
//        this.way = way;
//    }
//
//    private double CalculateBreak(double distToBrake, double speed) {
//        distToBrake = (distToBrake - speed * 1000 * 0.1 / 3600);
//
//        neuralNetwork.setInput(distToBrake, speed, 50);
//        //  neuralNetwork.setInput(0, 0, 0);
//        // calculate network
//        neuralNetwork.calculate();
//        // get network output
//        double[] networkOutput = neuralNetwork.getOutput();
//        return (int) networkOutput[0];
//    }
//}
//
//
///**
// *
// * @author Admin
// */
//public class TestRelativeNavigation {
//    
//    
//      public static VectorGeo2 PraseGPGGAFlatFile(String GPGGAString)
//    {
//               String strline=GPGGAString;
//
//               String[] parts = strline.split(Pattern.quote("|"));
//                String shirota = parts[4]; // 3723.465874
//                String p3 = parts[3]; // N
//                String dolgota = parts[5]; // 12202.26954
//                String p5 = parts[5]; // W
//    
//               return new VectorGeo2( Double.parseDouble(shirota),  Double.parseDouble(dolgota));
//  
//    }
//      
//     static double carBase=2.649;
//     static  double angle =0, x = 0, y= 0 ;
//    public static void UpdateCarXYAngleFromDynamics(double speed, double wheelAngle, double dt) {
//        wheelAngle = Math.toRadians(wheelAngle);
//        angle +=  speed*Math.tan(wheelAngle)*dt/carBase;
//        x += speed*Math.sin(angle)*dt;
//        y += speed*Math.cos(angle)*dt;        
//    }   
//         
//    
//    //updatePosition(25, 25, 90, 3, 0); 
//          
//     public static void updatePosition(double carX, double carY, double carAngle, double carSpeed, double distInFront) {
//         LinkedList<Vector2> way = null;
//         int currentWayPoint = 1;
//         way = new LinkedList<Vector2>();
//         Vector2 a1 = new Vector2(0.0f,   0.0f);
//         Vector2 a2 = new Vector2(100.0f, 100.0f);
//         Vector2 a3 = new Vector2(200.0f, 200.0f);
//         way.add(a1);
//         way.add(a2);
//         way.add(a3);
//        Vector2 gpsXYPosition = new Vector2((float) carX, (float) carY);
//         Vector2 distVector = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//        if (distVector.sub(gpsXYPosition).len() < 3) {
//            currentWayPoint++;
//            if (currentWayPoint == way.size()) {
//                currentWayPoint = 0;
//            }
//        }
//
//        int nextWaypoint = currentWayPoint + 1;
//        if (nextWaypoint == way.size()) {
//            nextWaypoint = 0;
//        }
//        int prevWayPoint = currentWayPoint - 1;
//        if (prevWayPoint == -1) {
//            prevWayPoint = way.size() - 1;
//        }
//
//        double nextWayAngle = GetNextWayAngle(  way.get(prevWayPoint), way.get(currentWayPoint));
//        nextWayAngle = nextWayAngle;
//        double angleDelta = nextWayAngle - carAngle;
//
//        if (angleDelta < -180) {
//            angleDelta = angleDelta + 2 * 180;
//        } else if (angleDelta > 180) {
//            angleDelta = angleDelta - 2 * 180;
//        }
//        double psy = angleDelta;
//
//        Vector2 prevPoint = way.get(prevWayPoint);
//        
//        Vector2 curentCopy = new Vector2(way.get(currentWayPoint).x, way.get(currentWayPoint).y);
//        double dist_error = GetDelta(way.get(prevWayPoint), curentCopy.sub(way.get(prevWayPoint)), gpsXYPosition).sub(gpsXYPosition).len();
//        Vector2 prev_a = way.get(prevWayPoint);
//        Vector2 current_b = way.get(currentWayPoint);
//        Vector2 my_c = gpsXYPosition;
//
//        if (((current_b.x - prev_a.x) * (my_c.y - prev_a.y) - (current_b.y - prev_a.y) * (my_c.x - prev_a.x)) < 0) {
//            dist_error = -dist_error;
//            psy = psy;
//        }
//        //var a = psy - Mathf.Rad2Deg * Mathf.Atan2(0.06 * dist_error  ,rigidbody.velocity.magnitude);
//        double wheelAngle = psy + Math.toDegrees(Math.atan2(0.06 * dist_error, carSpeed));
//
//        if (wheelAngle > 33) {
//            wheelAngle = 33;
//        }
//        if (wheelAngle < -33) {
//            wheelAngle = -33;
//        }
//
//      double steeringWheel = wheelAngle*14;
//        // this.acceleration=CalculateBreak(distInFront, carSpeed);      
//    }
//
//    private static double GetNextWayAngle(Vector2 prevPoint, Vector2 nextPoint) {
//        double deltaY = prevPoint.x - nextPoint.x;
//        double deltaX = prevPoint.y - nextPoint.y;
//        
//        
//        double _angle = Math.toDegrees(Math.atan2(nextPoint.x-prevPoint.x, nextPoint.y-prevPoint.y));
//        if(_angle < 0 ){
//         return  360+_angle ;
//        }
//        else
//        {
//            
//            return  _angle;
//        }
//    }
//
//    private static Vector2  GetDelta(Vector2 origin, Vector2 direction, Vector2 point) {
//        
//        Vector2 direction1 = new Vector2(direction.x, direction.y);
//        Vector2 point1 = new Vector2(point.x, point.y);
//        direction1.nor();
//        
//        Vector2 v = point1.sub(origin);
//        float d = Vector2.dot(v.x, v.y, direction1.x, direction1.y);
//        Vector2 returnVecor = new Vector2(origin.x, origin.y);
//        return returnVecor.add(direction1.x * d, direction1.y * d);
//    }
//    
//    
//      public static void main(String[] argv) {
//      
//          
//         updatePosition(0, -25, 0, 3, 0); 
//          
//        FileInputStream fis = null;
//        BufferedReader br = null;
//        WayProcessor wayProcessor = new WayProcessor();
//        CarDynamicsController  cDynController = new CarDynamicsController();
//        
//        wayProcessor.LoadWayNormalizedFromFileRealGPS("C:\\Users\\Admin\\Documents\\NetBeansProjects\\RobotAutomaticControl\\log1490975376874.txt");
//        cDynController.setWay( wayProcessor.GetWayNormilized());
//        String Name = "C:\\Users\\Admin\\Documents\\NetBeansProjects\\RobotAutomaticControl\\log1490975376874.txt";
//        
//        try {     
//            
//            double gpsSpeed = 0;
//            fis = new FileInputStream(Name);
//            //Construct BufferedReader from InputStreamReader
//            br = new BufferedReader(new InputStreamReader(fis));
//            String line = null;
//            String last_line = null;
//            String timeOld = "0";
//            int i=0;
//             Car car = new Car();
//              VectorGeo2 gpsPos=new VectorGeo2(0,0);
//              last_line  =br.readLine();
//              
//              
//              String last_shir_big ="0";
//              String last_lon_big ="0";
//              double xdiff=0;
//            while ((line = br.readLine()) != null) 
//            {i++;
////              if(i>10)
////              {
//                try{
//                    String strline=line;
//
//                    String[] parts = strline.split(Pattern.quote("|"));
//                   
//                    String time = parts[0]; // N
//                    //String gas = parts[3]; // N
//                    String steering = parts[2]; // N
//                    String speed = parts[1]; // N
//                    String shirota = parts[4]; // 3723.465874
//                    String dolgota = parts[5]; // 12202.26954
//    
//                    
//                    
//                    
//     
//                    
//                    VectorGeo2 coords =  new VectorGeo2( Double.parseDouble(shirota),  Double.parseDouble(shirota));
//                                       
//                    if(last_line != null)
//                    {                       
//                        parts = last_line.split(Pattern.quote("|"));
//                        String timeLast = parts[0]; // N
//                        //String gas = parts[3]; // N
//                        String steeringLast = parts[2]; // N
//                        String speedLast = parts[1]; // N
//                        String shirotaLast = parts[4]; // 3723.465874
//                        String dolgotaLast = parts[5]; // 12202.26954
//                        
//                        if(!shirotaLast.equals(shirota)|!dolgotaLast.equals(dolgota))
//                        {
//                            car.setLat(Double.parseDouble(shirota));
//                            car.setLon(Double.parseDouble(dolgota));
//                            gpsPos =  wayProcessor.getXYCarPositionNormilized(car); 
//                            xdiff = ( x - gpsPos.lon );
//                            System.out.println( "x diff = " + xdiff+ " y diff= " + ( y - gpsPos.lat) );
//                            x = gpsPos.lon;
//                            y = gpsPos.lat;
//                            angle  =Math.toRadians( MercatorUtil.bearing(Double.parseDouble(shirotaLast), Double.parseDouble(dolgotaLast),
//                                    Double.parseDouble(shirota), Double.parseDouble(dolgota) 
//                                    ));   
//                            
//                            double dy = MercatorUtil.getDistance( Double.parseDouble(last_shir_big), Double.parseDouble(last_lon_big),
//                                    Double.parseDouble(shirota), Double.parseDouble(dolgota) 
//                                    );
//                            
//                            //System.out.println("time=" + time+   "timeold="+ timeOld+ "dt = " + ((Long.parseLong(time)-Long.parseLong(timeOld))/1000.0));
//                            gpsSpeed = dy /((Long.parseLong(time)-Long.parseLong(timeOld))/1000.0);
//                            last_shir_big = shirota;
//                            last_lon_big = dolgota;
//                            timeOld = time;
//                            
//                            
//                           System.out.println(" GPS x = " + x + " GPS y = " + y + "GPSangle = " + Math.toDegrees( angle) );
//                            
//                            
//                            
//                            
//                        }
//                        else
//                        {                    
//                           // UpdateCarXYAngleFromDynamics(Double.parseDouble(speed)*1000/3600, Double.parseDouble(steering)/16,(Long.parseLong(time) - Long.parseLong(timeLast) )/1000.0);
//                            
////                            if(Math.abs(xdiff ) <1.2){
//                                UpdateCarXYAngleFromDynamics(gpsSpeed, Double.parseDouble(steering)/15,(Long.parseLong(time) - Long.parseLong(timeLast) )/1000.0);
////                            }else
////                            {
////                                UpdateCarXYAngleFromDynamics(Double.parseDouble(speed)*1000/3600, Double.parseDouble(steering)/15,(Long.parseLong(time) - Long.parseLong(timeLast) )/1000.0);
////                            }
//
//   System.out.println(" dynamics x = " + x + " dynamics y = "+y +" car angle = "+ Math.toDegrees(angle)+ " wheel angle = " +Double.parseDouble(steering)/14 +
//                                " speed = " +gpsSpeed +" dt = " + (Long.parseLong(time) - Long.parseLong(timeLast) )/1000.0);
//                                 
//                        }                   
//                    }
//                    
//                    last_line = line;
//
//                }catch(Exception ex)
//                {                    
//                    ex.printStackTrace();
//                }  
//                i=0;
//             // }
//            }   
//        } catch (Exception ex) {
//            Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//               br.close();
//            } catch (IOException ex) {
//                Logger.getLogger(WayProcessor.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } 
//    }
//}
