/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol.forms;

import helpers.MercatorUtil;
import org.lwjgl.opengl.GL11;
import programcontroller.SystemManager;
import robotautomaticcontrol.Renderer.RenderType;
import trafficobjects.Car;

public class CarForm extends TexturedRectangleShape {

	//Values to normalize car width and length. We must do it due to
	//texture is a square but vehicle has rectangle form. So real car picture 
	//is only a part of the texture
	private final float length_normalizer = 0.890625f;
	private final float width_normalizer = 0.421875f;
	
	public float length = 3.5f*1.3f*0.4f;
	public float width = 7.0f*0.5f*0.4f;
	public static final String textureLocation = "/textures/vehicles/carBasic.png"; 
	
	protected Car car;
	
	public CarForm(Car car) {
		super();
		this.car = car;
		this.set(0, 0, length, 0, 0, width);
		this.setTexture(textureLocation);
	}
	
	public void setDimensions(float w , float l) {
		length = l/ (2.0f * length_normalizer);
		width = w / (2.0f * width_normalizer);
		this.set(0, 0, length, 0, 0, width);
	}

	@Override
	public void applyRotation(float x, float y) {
		this.set(0, 0, x * length, y*length, - y*width, x*width);
	}
	
	@Override
	public void render(RenderType mode, Vector3d translation) {
		 
		//preRender();
		if (texture != null) texture.bind();
                GL11.glPushMatrix();
                GL11.glTranslated(MercatorUtil.LonToX(car.lon, 1)-translation.x , MercatorUtil.LatToY(car.lat,1)-translation.y, 0);
                //Attentio
                GL11.glRotatef((float)car.getDirection(), 0, 0, 1.0f);
		GL11.glBegin(GL11.GL_QUADS);
		//GL11.glColor3f(color.r, color.g, color.b);
		this.texturedVertex(1, 0, (float) -1, (float) 2);
		this.texturedVertex(1, 1, (float) 1, (float) 2);
		this.texturedVertex(0, 1, (float) 1, (float) -2);
		this.texturedVertex(0, 0, (float) -1, (float) -2);
		GL11.glEnd();
                GL11.glPopMatrix(); 
               // postRender();
		 
	}
	
	
	
}
