/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol.forms;

import robotautomaticcontrol.Renderer;

public interface IRenderable {

	public static enum FrustrumState {
		OutOfView,
		PartlyInView,
		InView
	}
	
	void render( Renderer.RenderType mode, Vector3d translation);
	 
}
