/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol.forms;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Color;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import programcontroller.SystemManager;
import robotautomaticcontrol.Renderer;

/**
 *
 * @author Мади
 */
public class PointXY implements IRenderable{
    
    
	Vector3double begin = null;

    public Vector3double getBegin() {
        
        return begin;
    }

    public void setBegin(Vector3double begin) {
        this.begin = begin;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
         
        
        Color color = null;
	
       public  int pointID;
	 
	
	public PointXY(Vector3double begin , Color color, int pointID) {
            this.begin = begin ;
            this.color = color;
            this.pointID = pointID;
		 		 
	}

	
	@Override
	public void render(Renderer.RenderType mode, Vector3d translation) {
		if (mode == Renderer.RenderType.Picking) {
			GL11.glPushName(SystemManager.getInstance().getPickingHandler().addObject(this));
					
                }
	
		GL11.glBegin(GL11.GL_LINE_STRIP);
		
                double dxa = (begin.x-translation.x-0.1);
                double dxb = (begin.x-translation.x+0.1);
                
                GL11.glVertex2d( dxa,  begin.y-translation.y);
		GL11.glVertex2d( dxb,  begin.y-translation.y);
                
		GL11.glEnd();
                GL11.glBegin(GL11.GL_LINE_STRIP);
		
                
                double dya =  (begin.y-0.1-translation.y);
                double dyb =  (begin.y+0.1-translation.y);
                GL11.glVertex2d( begin.x-translation.x, dya);
		GL11.glVertex2d( begin.x-translation.x, dyb);
                
		GL11.glEnd();
               
                
                if (mode == Renderer.RenderType.Picking) {
			GL11.glPopName();
		}
        }
                 
		 
    
}
