/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol.forms;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Color;
import org.lwjgl.util.vector.Vector2f;
import programcontroller.SystemManager;
import robotautomaticcontrol.Renderer;
import trafficobjects.Car;

/**
 *
 * @author Мади
 */
public class SegmentXY implements IRenderable{
    
    
       Vector3d begin = null;
       
        Vector3d end = null;
        
        Color color = null;
	
	 
	
	public SegmentXY(Vector3d begin, Vector3d end, Color color) {
            this.begin=begin;
            this.end=end;
            this.color=color;
		 		 
	}

	
	@Override
	public void render(Renderer.RenderType mode, Vector3d translation) {
		 
                GL11.glColor3f(0.0f, 0.5f, 0.0f);
		GL11.glBegin(GL11.GL_LINE_STRIP);
		
                GL11.glVertex2d(begin.x-translation.x, begin.y-translation.y);
		GL11.glVertex2d(end.x-translation.x, end.y-translation.y);
		GL11.glEnd();
                        
            // R,G,B,A Set The Color To Blue One Time Only
        

         
				 
	}
   
}
