/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotautomaticcontrol.visualizer;

public class TextureManager {
	private static TextureManager instance; 
	
	private TextureLoader loader;
	
	private TextureManager(){
		loader = new TextureLoader();
	}
	
	public static TextureManager getInstance()
	{
		if (instance == null) instance = new TextureManager();
		return instance;
	}
	
	public TextureLoader getLoader()
	{
		return loader;
	}
	
//	public Texture renderTotexture(IRenderable r){
//		loader.createTexture(name, width, height);
//	}
	
}