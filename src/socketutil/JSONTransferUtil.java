/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketutil;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;
import types.DataFromCar;
import types.DataFromControlAndroid;

/**
 *
 * @author Мади
 */
public class JSONTransferUtil {
     public static String CarDataToString(DataFromCar dfc)
    {
        String json =null;
         try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("EngineRPM", String.valueOf(dfc.getEngineRPM())); // Set the first name/pair 
            jsonObj.put("Speed", String.valueOf( dfc.getSpeed())); 
            json=jsonObj.toString();
         } catch(JSONException ex) {
	        ex.printStackTrace();
	 }
        return json;
    }
       
    public static String AndroidDataToString(DataFromControlAndroid dfc)
    {
        String json =null;
         try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("AcceleratioRate", String.valueOf(dfc.getAcceleratioRate())); // Set the first name/pair 
            jsonObj.put("SteeringWheelRate", String.valueOf( dfc.getSteeringWheelRate())); 
            json=jsonObj.toString();
         } catch(JSONException ex) {
	        ex.printStackTrace();
	 }
        return json;
    
    }
    
    public static  DataFromControlAndroid StringToAndroidData(String FromAndroidString)
    {        
        try {
                JSONObject jsonObj = new JSONObject(FromAndroidString);
		DataFromControlAndroid mb = new DataFromControlAndroid();
                mb.setAcceleratioRate(jsonObj.getInt("AcceleratioRate"));
                mb.setSteeringWheelRate(jsonObj.getInt("SteeringWheelRate"));
                mb.setDriveKPP(jsonObj.getString("KPP"));
                return mb;              
        } catch(JSONException ex) {
	        ex.printStackTrace();
	}              
                return null;       
    }
    
    public static  DataFromCar StringToCarData(String FromCarString)
    {        
                try {
                    JSONObject jsonObj = new JSONObject(FromCarString);
                    DataFromCar mb = new DataFromCar();
                    mb.setEngineRPM(jsonObj.getInt("EngineRPM"));
                    mb.setSpeed(jsonObj.getInt("Speed"));
                    return mb;              
                } catch(JSONException ex) {
                        ex.printStackTrace();
                }              
                return null;      
    }
    
}
