/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketutil;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.InputStream;
import java.io.InputStreamReader;
import static java.lang.System.in;

/**
 *
 * @author Мади
 */
public class SocketHelperCommunicator {
    
    public static void _send(OutputStream outStream, byte buf[])
    {
        try {
            String message= buf.length+ "\n";
            outStream.write(message.getBytes());                        
             
            
            outStream.write(buf);
            outStream.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }  
    
    public static String _recv(BufferedReader reader)
    {
        String length_str="";
        
         
        
        try {
            length_str=reader.readLine();
            
        } catch (IOException ex) {
            Logger.getLogger(SocketHelperCommunicator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int totalLength = Integer.parseInt(length_str);
        
        char[] objData = new char[totalLength];
        
        
        try {
            reader.read(objData, 0, totalLength);
            
        } catch (IOException ex) {
            Logger.getLogger(SocketHelperCommunicator.class.getName()).log(Level.SEVERE, null, ex);
        }
         String output = new  String(objData, 0, totalLength);   
        return  output;
        
    }
    
}
