/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;



import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import helpers.VectorGeo2;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
/**
 *
 * @author Мади
 */
public class GPSTestReader {
public GPSTestReader()
    {
        super();
    }
    
    void connect ( String portName ) throws Exception
    {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if ( portIdentifier.isCurrentlyOwned() )
        {
            System.out.println("Error: Port is currently in use");
        }
        else
        {
            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
            
            if ( commPort instanceof SerialPort )
            {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                
                InputStream in = serialPort.getInputStream();
                OutputStream out = serialPort.getOutputStream();
                
                (new Thread(new SerialReader(in))).start();
                (new Thread(new SerialWriter(out))).start();

            }
            else
            {
                System.out.println("Error: Only serial ports are handled by this example.");
            }
        }     
    }
    
    /** */
    public static class SerialReader implements Runnable 
    {
        InputStream in;
        
        public SerialReader ( InputStream in )
        {
            this.in = in;
        }
        
        public void run ()
        {
            byte[] buffer = new byte[1024];
            int len = -1;
            try
            {
                while ( ( len = this.in.read(buffer)) > -1 )
                {
                    System.out.print(new String(buffer,0,len));
                }
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }            
        }
    }

    /** */
    public static class SerialWriter implements Runnable 
    {
        OutputStream out;
        
        public SerialWriter ( OutputStream out )
        {
            this.out = out;
        }
        
        public void run ()
        {
            try
            {                
                int c = 0;
                 
                   // this.out.write("log gpgga ontime 0.1\r\n".getBytes(StandardCharsets.US_ASCII));
                   
                    this.out.flush();
                               
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }            
        }
    }//log gpgga ontime 0
    //$GPGGA,134658.00,5106.9792,N,11402.3003,W,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60
    
    
    
    
    //$GPGGA,115113.70,5548.1697,N,03731.8237,E,1,12,1.1,-4973005.49,M,4973178.00,M,,*78

    
    
    public static VectorGeo2 PraseGPGGA(String GPGGAString)
    {
               String strline=GPGGAString;

               String shirota = strline.substring(strline.indexOf(",", 15)+1, strline.indexOf(",", 25));
               String dolgota = strline.substring(strline.indexOf(",", 29)+1, strline.indexOf(",", 32));
    
               return new VectorGeo2(Double.parseDouble(shirota), Double.parseDouble(dolgota));
  
    }
    
    public static void main ( String[] args )
    {
        try
        {
            
            
            (new GPSTestReader()).connect("COM7");
            
        }
        catch ( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    
    }
}
    
