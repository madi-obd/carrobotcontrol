/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;


import java.io.Serializable;

/**
 *
 * @author Мади
 */
public class DataFromControlAndroid implements Serializable  {
    private int acceleratioRate=0;
    private int steeringWheelRate=0;

        private String driveKPP="P";

    public String getDriveKPP() {
        return driveKPP;
    }

    public void setDriveKPP(String driveKPP) {
        this.driveKPP = driveKPP;
    }
    public void setAcceleratioRate(int acceleratioRate) {
        this.acceleratioRate = acceleratioRate;
    }

    public void setSteeringWheelRate(int steeringWheelRate) {
        this.steeringWheelRate = steeringWheelRate;
    }

    public int getAcceleratioRate() {
        return acceleratioRate;
    }

    public int getSteeringWheelRate() {
        return steeringWheelRate;
    }
    
     
}
